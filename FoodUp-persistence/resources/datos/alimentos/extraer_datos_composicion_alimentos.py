# -*- coding: utf-8 -*-
import urllib2,unicodedata
from bs4 import BeautifulSoup
 
#método de análisis de una dirección web
def analisisDescarga(archivo,conexion):
    html = conexion.read()
    soup = BeautifulSoup(html)
    #obtenemos una lista de String con la condición de atributos class con valores details y price
    links = soup.find_all(True, {'align':['left','right']})
    #la lista alterna valores de nombre de producto y precio
    #   creamos una bandera para diferenciar si es valor o producto
    value = False
    for tag in links:
        print("--")
        for linea in tag:
            linea = linea.strip();
            #adaptamos unicode a utf-8
            normalizado=unicodedata.normalize('NFKD', linea).encode('ascii','ignore')
 
            if len(normalizado)>1:
                if value:
                    print('valor: '+normalizado)
                    value = False
                    archivo.write(normalizado+'\n')
                else:
                    print('composicion: '+normalizado)
                    value = True
                    archivo.write(normalizado+'\t')
#este método se conectará con la web y establece un timeout que obliga a reintentar el fallo
def preparar(archivo,web):
    print(web)
    req = urllib2.Request(web)
    req.add_header('User-agent','Mozilla/5.0')
    result = urllib2.urlopen(req,timeout=10)#timeout de 10 segundos
    analisisDescarga(archivo,result)
#        preparar(archivo,web)
#Programa principal
print('Comienza el programa')
salir = False
while not salir:
    archivo = open(raw_input("Nombre del alimento: ")+'.csv','w')
    id_alim = raw_input("Id alimento: ")
    url = 'http://www.ienva.org/CalcDieta/composicion_sub_result.php?q=' + id_alim + '&r='
    archivo.write('GENERAL'+'\n')
    preparar(archivo,url + 'GENERAL' + '&s=1')
    archivo.write('MINERALES'+'\n')
    preparar(archivo,url + 'MINERALES' + '&s=1')
    archivo.write('VITAMINAS'+'\n')
    preparar(archivo,url + 'VITAMINAS' + '&s=1')
    archivo.write('ACIDOS_GRASOS'+'\n')
    preparar(archivo,url + 'ACIDOS_GRASOS' + '&s=1')
    archivo.close()
print('Fin del programa')
