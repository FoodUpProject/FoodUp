# -*- coding: utf-8 -*-
import urllib2,unicodedata
from bs4 import BeautifulSoup
 
#método de análisis de una dirección web
def analisisDescarga(archivo,conexion):
    html = conexion.read()
    soup = BeautifulSoup(html)
    #obtenemos una lista de String con la condición de atributos class con valores details y price
    links = soup.find_all(True, {'class':['details','price','pricePerKilogram']})
    #la lista alterna valores de nombre de producto y precio
    #   creamos una bandera para diferenciar si es valor o producto
    precio = False
    precioUni = False
    for tag in links:
        print("--")
        for linea in tag:
            linea = linea.strip();
            #adaptamos unicode a utf-8
            normalizado=unicodedata.normalize('NFKD', linea).encode('ascii','ignore')
 
            if len(normalizado)>1:
                if precio:
                    print('precio: '+normalizado)
                    precioUni = not precioUni
                    precio= not precio
                    archivo.write(normalizado+'\t')
                elif precioUni:
                    print('precio unitario: '+normalizado)
                    precioUni = not precioUni
                    archivo.write(normalizado+'\n')
                else:
		    if normalizado == '0,00':
                        continue
                    print('producto: '+linea)
                    precio = not precio
                    archivo.write(normalizado+'\t')
#este método se conectará con la web y establece un timeout que obliga a reintentar el fallo
def preparar(archivo,web):
    try:
        print(web)
        conector = urllib2.urlopen(web,timeout=10)#timeout de 10 segundos
        analisisDescarga(archivo,conector)
    except:
        print("Tiempo de espera agotado, volviendo a intentar")
        preparar(archivo,web)
 
#Programa principal
print('Comienza el programa')
salir = False
while not salir:
    keyinput = raw_input("1 una pag, 2 varias, 0 salir: ") #esta linea es la que te permite leer desde el teclado
    try:
        keyinput = int(keyinput)
    except ValueError:
        print("Opción inválida")
        continue
    if keyinput == 0:
        salir = True
        print("Adios")
    elif keyinput == 1:
        archivo = open(raw_input("Nombre del archivo: ")+'.csv','w')
        urlin = raw_input("Url: ")
        preparar(archivo,urlin)
        archivo.close()
    elif keyinput == 2:
        archivo = open(raw_input("Nombre del archivo: ")+'.csv','w')
        urlin = raw_input("Url: ")
        numpag = raw_input("Num_pag: ")
        try:
            numpag = int(numpag)
        except ValueError:
            print("Opción inválida")
            archivo.close()
            continue
        for x in range(0,numpag):
            url = urlin+str(x)+'&disp=grid'
            preparar(archivo,url)
        archivo.close()
print('Fin del programa')
