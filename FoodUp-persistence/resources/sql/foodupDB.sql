--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aliments; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE aliments (
    id integer NOT NULL,
    name_aliment text NOT NULL,
    id_picture integer,
    food_group integer NOT NULL,
    quantity_base numeric DEFAULT 100 NOT NULL,
    unit integer DEFAULT 1 NOT NULL,
    agua_g numeric DEFAULT 0,
    energia_kcal numeric DEFAULT 0,
    energia_kj numeric DEFAULT 0,
    proteinas_g numeric DEFAULT 0,
    grasa_total_g numeric DEFAULT 0,
    hidratos_total_g numeric DEFAULT 0,
    azucares_g numeric DEFAULT 0,
    almidon_g numeric DEFAULT 0,
    fibra_dietetica_g numeric DEFAULT 0,
    grasa_saturada_g numeric DEFAULT 0,
    grasa_monoinsaturada_g numeric DEFAULT 0,
    grasa_poliinsaturada_g numeric DEFAULT 0,
    colesterol_mg numeric DEFAULT 0,
    alcohol_g numeric DEFAULT 0,
    calcio_mg numeric DEFAULT 0,
    hierro_mg numeric DEFAULT 0,
    yodo_g numeric DEFAULT 0,
    magnesio_mg numeric DEFAULT 0,
    cinc_mg numeric DEFAULT 0,
    sodio_mg numeric DEFAULT 0,
    potasio_mg numeric DEFAULT 0,
    fosforo_mg numeric DEFAULT 0,
    selenio_g numeric DEFAULT 0,
    tiamina_o_vit_b1_mg numeric DEFAULT 0,
    riboflavina_o_vit_b2_mg numeric DEFAULT 0,
    eq_niacina_mg numeric DEFAULT 0,
    vit_b6_mg numeric DEFAULT 0,
    acido_folico_g numeric DEFAULT 0,
    vit_b12_g numeric DEFAULT 0,
    vit_c_mg numeric DEFAULT 0,
    vit_a_eq_retinol_g numeric DEFAULT 0,
    retinol_g numeric DEFAULT 0,
    carotenos_g numeric DEFAULT 0,
    vit_d_g numeric DEFAULT 0,
    vit_e_mg numeric DEFAULT 0,
    c12_0_g numeric DEFAULT 0,
    c14_0_g numeric DEFAULT 0,
    c16_0_g numeric DEFAULT 0,
    c18_0_g numeric DEFAULT 0,
    c18_1_g numeric DEFAULT 0,
    c18_2_g numeric DEFAULT 0,
    c18_3_g numeric DEFAULT 0,
    c20_4_g numeric DEFAULT 0,
    c20_5_g numeric DEFAULT 0,
    c22_6_g numeric DEFAULT 0,
    acidos_grasos_cis_g numeric DEFAULT 0,
    acidos_grasos_trans_g numeric DEFAULT 0
);


ALTER TABLE public.aliments OWNER TO foodup_psql;

--
-- Name: aliments_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE aliments_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aliments_seq OWNER TO foodup_psql;

--
-- Name: dishes; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE dishes (
    id integer NOT NULL,
    name_dish text NOT NULL,
    id_picture integer,
    duration interval NOT NULL,
    video text,
    recipe text,
    posible_position integer NOT NULL,
    diners integer DEFAULT 1 NOT NULL,
    id_user_owner integer NOT NULL,
    id_base_dish integer NOT NULL
);


ALTER TABLE public.dishes OWNER TO foodup_psql;

--
-- Name: dishes_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE dishes_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dishes_seq OWNER TO foodup_psql;

--
-- Name: food_groups; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE food_groups (
    id integer NOT NULL,
    name_group text NOT NULL,
    id_parent integer
);


ALTER TABLE public.food_groups OWNER TO foodup_psql;

--
-- Name: foodup_types; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE foodup_types (
    id integer NOT NULL,
    name_type text NOT NULL
);


ALTER TABLE public.foodup_types OWNER TO foodup_psql;

--
-- Name: foodups; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE foodups (
    id integer NOT NULL,
    name text NOT NULL,
    diners integer NOT NULL,
    color integer DEFAULT 0,
    init_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    days_of_menu integer DEFAULT 30,
    foodup_type integer NOT NULL,
    foodup_group_id integer DEFAULT (-1) NOT NULL,
    following_foodup_id integer NOT NULL,
    id_user_owner integer NOT NULL
);


ALTER TABLE public.foodups OWNER TO foodup_psql;

--
-- Name: foodups_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE foodups_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.foodups_seq OWNER TO foodup_psql;

--
-- Name: ingredients; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE ingredients (
    id_aliment integer NOT NULL,
    id_dish integer NOT NULL,
    quantity numeric NOT NULL,
    unit integer NOT NULL,
    main_ingredient boolean DEFAULT false NOT NULL
);


ALTER TABLE public.ingredients OWNER TO foodup_psql;

--
-- Name: meals; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE meals (
    id integer NOT NULL,
    id_foodup integer NOT NULL,
    date_day timestamp without time zone NOT NULL,
    position_of_day integer NOT NULL
);


ALTER TABLE public.meals OWNER TO foodup_psql;

--
-- Name: meals_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE meals_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meals_seq OWNER TO foodup_psql;

--
-- Name: my_coleguis; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE my_coleguis (
    id_user integer NOT NULL,
    id_colegui integer NOT NULL
);


ALTER TABLE public.my_coleguis OWNER TO foodup_psql;

--
-- Name: pictures; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE pictures (
    id integer NOT NULL,
    file_picture bytea NOT NULL
);


ALTER TABLE public.pictures OWNER TO foodup_psql;

--
-- Name: pictures_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE pictures_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_seq OWNER TO foodup_psql;

--
-- Name: positions; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE positions (
    id integer NOT NULL,
    name_type text NOT NULL
);


ALTER TABLE public.positions OWNER TO foodup_psql;

--
-- Name: relation_dishes_meals; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE relation_dishes_meals (
    foodup_group_id integer,
    id_dish integer NOT NULL,
    id_meal integer NOT NULL,
    dish_position integer NOT NULL
);


ALTER TABLE public.relation_dishes_meals OWNER TO foodup_psql;

--
-- Name: relation_foodups_users; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE relation_foodups_users (
    id_foodup integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.relation_foodups_users OWNER TO foodup_psql;

--
-- Name: units; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE units (
    id integer NOT NULL,
    code_unit text NOT NULL,
    name_unit text
);


ALTER TABLE public.units OWNER TO foodup_psql;

--
-- Name: user_pictures; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE user_pictures (
    id_user integer NOT NULL,
    id_dish integer NOT NULL,
    picture_file bytea NOT NULL
);


ALTER TABLE public.user_pictures OWNER TO foodup_psql;

--
-- Name: users; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    age integer,
    name_user text DEFAULT 'user default'::text NOT NULL,
    email text,
    password text,
    last_update timestamp without time zone
);


ALTER TABLE public.users OWNER TO foodup_psql;

--
-- Name: users_restriction; Type: TABLE; Schema: public; Owner: foodup_psql; Tablespace: 
--

CREATE TABLE users_restriction (
    id_user integer NOT NULL,
    id_aliment integer DEFAULT (-1) NOT NULL,
    food_group integer DEFAULT (-1) NOT NULL
);


ALTER TABLE public.users_restriction OWNER TO foodup_psql;

--
-- Name: users_seq; Type: SEQUENCE; Schema: public; Owner: foodup_psql
--

CREATE SEQUENCE users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_seq OWNER TO foodup_psql;

--
-- Data for Name: aliments; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY aliments (id, name_aliment, id_picture, food_group, quantity_base, unit, agua_g, energia_kcal, energia_kj, proteinas_g, grasa_total_g, hidratos_total_g, azucares_g, almidon_g, fibra_dietetica_g, grasa_saturada_g, grasa_monoinsaturada_g, grasa_poliinsaturada_g, colesterol_mg, alcohol_g, calcio_mg, hierro_mg, yodo_g, magnesio_mg, cinc_mg, sodio_mg, potasio_mg, fosforo_mg, selenio_g, tiamina_o_vit_b1_mg, riboflavina_o_vit_b2_mg, eq_niacina_mg, vit_b6_mg, acido_folico_g, vit_b12_g, vit_c_mg, vit_a_eq_retinol_g, retinol_g, carotenos_g, vit_d_g, vit_e_mg, c12_0_g, c14_0_g, c16_0_g, c18_0_g, c18_1_g, c18_2_g, c18_3_g, c20_4_g, c20_5_g, c22_6_g, acidos_grasos_cis_g, acidos_grasos_trans_g) FROM stdin;
\.


--
-- Name: aliments_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('aliments_seq', 1, false);


--
-- Data for Name: dishes; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY dishes (id, name_dish, id_picture, duration, video, recipe, posible_position, diners, id_user_owner, id_base_dish) FROM stdin;
\.


--
-- Name: dishes_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('dishes_seq', 1, false);


--
-- Data for Name: food_groups; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY food_groups (id, name_group, id_parent) FROM stdin;
1	Aceites y grasas	\N
2	Aceites vegetales	1
3	Grasas	1
4	Aperitivos	\N
5	A base de cereales	4
6	Otros aperitivos	4
7	Azúcares, dulces y derivados	\N
8	Azúcares, miel y jarabes	7
9	Caramelos y golosinas	7
10	Chocolates y similares	7
11	Mermeladas y jaleas	7
12	Otros dulces	7
13	Turrones y mazapanes	7
14	Bebidas sin alcohol	\N
15	Aguas	14
16	Café, té e infusiones	14
17	Otras bebidas sin alcohol	14
18	Refrescos	14
19	Zumos y néctares comerciales	14
20	Carnes	\N
21	Aves	20
22	Cerdo	20
23	Cordero	20
24	Embutidos y otros productos cárnicos	20
25	Otras carnes	20
26	Vacuno	20
27	Vísceras	20
28	Cereales	\N
29	Bollería y pastelería	28
30	Galletas	28
31	Granos y harinas	28
32	Panaderia	28
33	Pasta	28
34	Cervezas y sidras	\N
35	Frutas	\N
36	Frutas desecadas	35
37	Frutas en conserva	35
38	Frutas frescas	35
39	Frutos secos y semillas	35
40	Huevos	\N
41	Huevos de gallina	40
42	Otros huevos	40
43	Legumbres	\N
44	Derivados de legumbres	43
45	Legumbres en conserva	43
46	Legumbres frescas o congeladas	43
47	Legumbres secas	43
48	Licores y aguardientes	\N
49	Lácteos	\N
50	Batidos lácteos	49
51	Leche	49
52	Nata	49
53	Postres y lácteos	49
54	Quesos	49
55	Yogures y leches fermentadas	49
56	Pescados	\N
57	Moluscos y crustáceos en conserva	56
58	Moluscos y crustáceos frescos o congelados	56
59	Otros productos y derivados de pescados	56
60	Pescados en conserva	56
61	Pescados en salazón o ahumados	56
62	Pescados frescos o congelados	56
63	Platos preparados y precocinados	\N
64	Platos preparados	63
65	Precocinados congelados	63
66	Precocinados en conserva	63
67	Sopas y cremas	63
68	Salsas, condimentos, hierbas y especias	\N
69	Condimentos y especias	68
70	Salsa	68
71	Varios	\N
72	Levaduras	71
73	Verduras, hortalizas y derivados	\N
74	Algas y derivados	73
75	Setas	73
76	Tubérculos y derivados	73
77	Verduras y hortalizas en conserva	73
78	Verduras y hortalizas frescas o congeladas	73
79	Vinos	\N
\.


--
-- Data for Name: foodup_types; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY foodup_types (id, name_type) FROM stdin;
\.


--
-- Data for Name: foodups; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY foodups (id, name, diners, color, init_date, end_date, days_of_menu, foodup_type, foodup_group_id, following_foodup_id, id_user_owner) FROM stdin;
\.


--
-- Name: foodups_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('foodups_seq', 1, false);


--
-- Data for Name: ingredients; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY ingredients (id_aliment, id_dish, quantity, unit, main_ingredient) FROM stdin;
\.


--
-- Data for Name: meals; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY meals (id, id_foodup, date_day, position_of_day) FROM stdin;
\.


--
-- Name: meals_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('meals_seq', 1, false);


--
-- Data for Name: my_coleguis; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY my_coleguis (id_user, id_colegui) FROM stdin;
\.


--
-- Data for Name: pictures; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY pictures (id, file_picture) FROM stdin;
\.


--
-- Name: pictures_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('pictures_seq', 1, false);


--
-- Data for Name: positions; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY positions (id, name_type) FROM stdin;
\.


--
-- Data for Name: relation_dishes_meals; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY relation_dishes_meals (foodup_group_id, id_dish, id_meal, dish_position) FROM stdin;
\.


--
-- Data for Name: relation_foodups_users; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY relation_foodups_users (id_foodup, id_user) FROM stdin;
\.


--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY units (id, code_unit, name_unit) FROM stdin;
1	g	gramos
\.


--
-- Data for Name: user_pictures; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY user_pictures (id_user, id_dish, picture_file) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY users (id, age, name_user, email, password, last_update) FROM stdin;
\.


--
-- Data for Name: users_restriction; Type: TABLE DATA; Schema: public; Owner: foodup_psql
--

COPY users_restriction (id_user, id_aliment, food_group) FROM stdin;
\.


--
-- Name: users_seq; Type: SEQUENCE SET; Schema: public; Owner: foodup_psql
--

SELECT pg_catalog.setval('users_seq', 1, false);


--
-- Name: aliments_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY aliments
    ADD CONSTRAINT aliments_pkey PRIMARY KEY (id);


--
-- Name: dishes_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY dishes
    ADD CONSTRAINT dishes_pkey PRIMARY KEY (id);


--
-- Name: food_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY food_groups
    ADD CONSTRAINT food_groups_pkey PRIMARY KEY (id);


--
-- Name: foodup_types_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY foodup_types
    ADD CONSTRAINT foodup_types_pkey PRIMARY KEY (id);


--
-- Name: foodups_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY foodups
    ADD CONSTRAINT foodups_pkey PRIMARY KEY (id);


--
-- Name: ingredients_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id_aliment, id_dish);


--
-- Name: meals_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY meals
    ADD CONSTRAINT meals_pkey PRIMARY KEY (id);


--
-- Name: my_coleguis_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY my_coleguis
    ADD CONSTRAINT my_coleguis_pkey PRIMARY KEY (id_user, id_colegui);


--
-- Name: pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- Name: positions_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);


--
-- Name: relation_foodups_users_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY relation_foodups_users
    ADD CONSTRAINT relation_foodups_users_pkey PRIMARY KEY (id_foodup, id_user);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: user_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY user_pictures
    ADD CONSTRAINT user_pictures_pkey PRIMARY KEY (id_user, id_dish);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: foodup_psql; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: aliments_food_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY aliments
    ADD CONSTRAINT aliments_food_group_fkey FOREIGN KEY (food_group) REFERENCES food_groups(id) ON UPDATE CASCADE;


--
-- Name: aliments_id_picture_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY aliments
    ADD CONSTRAINT aliments_id_picture_fkey FOREIGN KEY (id_picture) REFERENCES pictures(id) ON UPDATE CASCADE;


--
-- Name: aliments_unit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY aliments
    ADD CONSTRAINT aliments_unit_fkey FOREIGN KEY (unit) REFERENCES units(id) ON UPDATE CASCADE;


--
-- Name: dishes_id_base_dish_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY dishes
    ADD CONSTRAINT dishes_id_base_dish_fkey FOREIGN KEY (id_base_dish) REFERENCES dishes(id) ON UPDATE CASCADE;


--
-- Name: dishes_id_picture_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY dishes
    ADD CONSTRAINT dishes_id_picture_fkey FOREIGN KEY (id_picture) REFERENCES pictures(id) ON UPDATE CASCADE;


--
-- Name: dishes_id_user_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY dishes
    ADD CONSTRAINT dishes_id_user_owner_fkey FOREIGN KEY (id_user_owner) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: dishes_posible_position_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY dishes
    ADD CONSTRAINT dishes_posible_position_fkey FOREIGN KEY (posible_position) REFERENCES positions(id) ON UPDATE CASCADE;


--
-- Name: food_groups_id_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY food_groups
    ADD CONSTRAINT food_groups_id_parent_fkey FOREIGN KEY (id_parent) REFERENCES food_groups(id) ON UPDATE CASCADE;


--
-- Name: foodups_following_foodup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY foodups
    ADD CONSTRAINT foodups_following_foodup_id_fkey FOREIGN KEY (following_foodup_id) REFERENCES foodups(id) ON UPDATE CASCADE;


--
-- Name: foodups_foodup_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY foodups
    ADD CONSTRAINT foodups_foodup_type_fkey FOREIGN KEY (foodup_type) REFERENCES foodup_types(id) ON UPDATE CASCADE;


--
-- Name: foodups_id_user_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY foodups
    ADD CONSTRAINT foodups_id_user_owner_fkey FOREIGN KEY (id_user_owner) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ingredients_id_aliment_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_id_aliment_fkey FOREIGN KEY (id_aliment) REFERENCES aliments(id) ON UPDATE CASCADE;


--
-- Name: ingredients_id_dish_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_id_dish_fkey FOREIGN KEY (id_dish) REFERENCES dishes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ingredients_unit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_unit_fkey FOREIGN KEY (unit) REFERENCES units(id) ON UPDATE CASCADE;


--
-- Name: meals_id_foodup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY meals
    ADD CONSTRAINT meals_id_foodup_fkey FOREIGN KEY (id_foodup) REFERENCES foodups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: meals_position_of_day_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY meals
    ADD CONSTRAINT meals_position_of_day_fkey FOREIGN KEY (position_of_day) REFERENCES positions(id) ON UPDATE CASCADE;


--
-- Name: my_coleguis_id_colegui_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY my_coleguis
    ADD CONSTRAINT my_coleguis_id_colegui_fkey FOREIGN KEY (id_colegui) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: my_coleguis_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY my_coleguis
    ADD CONSTRAINT my_coleguis_id_user_fkey FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relation_dishes_meals_dish_position_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY relation_dishes_meals
    ADD CONSTRAINT relation_dishes_meals_dish_position_fkey FOREIGN KEY (dish_position) REFERENCES positions(id) ON UPDATE CASCADE;


--
-- Name: relation_dishes_meals_id_dish_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY relation_dishes_meals
    ADD CONSTRAINT relation_dishes_meals_id_dish_fkey FOREIGN KEY (id_dish) REFERENCES dishes(id) ON UPDATE CASCADE;


--
-- Name: relation_dishes_meals_id_meal_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY relation_dishes_meals
    ADD CONSTRAINT relation_dishes_meals_id_meal_fkey FOREIGN KEY (id_meal) REFERENCES meals(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relation_foodups_users_id_foodup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY relation_foodups_users
    ADD CONSTRAINT relation_foodups_users_id_foodup_fkey FOREIGN KEY (id_foodup) REFERENCES foodups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relation_foodups_users_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY relation_foodups_users
    ADD CONSTRAINT relation_foodups_users_id_user_fkey FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: user_pictures_id_dish_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY user_pictures
    ADD CONSTRAINT user_pictures_id_dish_fkey FOREIGN KEY (id_dish) REFERENCES dishes(id) ON UPDATE CASCADE;


--
-- Name: user_pictures_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY user_pictures
    ADD CONSTRAINT user_pictures_id_user_fkey FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_restriction_food_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY users_restriction
    ADD CONSTRAINT users_restriction_food_group_fkey FOREIGN KEY (food_group) REFERENCES food_groups(id) ON UPDATE CASCADE;


--
-- Name: users_restriction_id_aliment_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY users_restriction
    ADD CONSTRAINT users_restriction_id_aliment_fkey FOREIGN KEY (id_aliment) REFERENCES aliments(id) ON UPDATE CASCADE;


--
-- Name: users_restriction_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: foodup_psql
--

ALTER TABLE ONLY users_restriction
    ADD CONSTRAINT users_restriction_id_user_fkey FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

