DROP TABLE IF EXISTS user_pictures;
DROP TABLE IF EXISTS users_restriction;
DROP TABLE IF EXISTS relation_dishes_meals;
DROP TABLE IF EXISTS relation_foodups_users;
DROP TABLE IF EXISTS meals;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS dishes;
DROP TABLE IF EXISTS aliments;
DROP TABLE IF EXISTS pictures;
DROP TABLE IF EXISTS my_coleguis;
DROP TABLE IF EXISTS foodups;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS user_types;
DROP TABLE IF EXISTS foodup_types;
DROP TABLE IF EXISTS food_groups;
DROP TABLE IF EXISTS positions;
DROP TABLE IF EXISTS units;

DROP SEQUENCE IF EXISTS users_seq;
DROP SEQUENCE IF EXISTS foodups_seq;
DROP SEQUENCE IF EXISTS dishes_seq;
DROP SEQUENCE IF EXISTS pictures_seq;
DROP SEQUENCE IF EXISTS aliments_seq;
DROP SEQUENCE IF EXISTS meals_seq;

CREATE TABLE units (
id integer PRIMARY KEY,
code_unit text NOT NULL,
name_unit text
);

CREATE TABLE positions (
id integer PRIMARY KEY,
name_type text NOT NULL
);

CREATE TABLE food_groups (
id integer PRIMARY KEY,
name_group text NOT NULL,
id_parent integer REFERENCES food_groups(id) ON UPDATE CASCADE
);

CREATE TABLE foodup_types (
id integer PRIMARY KEY,
name_type text NOT NULL
);

CREATE TABLE users (
id integer PRIMARY KEY,
age int,
name_user text NOT NULL DEFAULT 'user default',
email text,
password text,
last_update timestamp
);

CREATE TABLE foodups (
id integer PRIMARY KEY,
name text NOT NULL,
diners integer NOT NULL,
color integer DEFAULT 0,
init_date timestamp NOT NULL,
end_date timestamp,
days_of_menu integer DEFAULT 30,
foodup_type integer NOT NULL REFERENCES foodup_types(id) ON UPDATE CASCADE,
foodup_group_id integer NOT NULL DEFAULT -1,
following_foodup_id integer NOT NULL REFERENCES foodups(id) ON UPDATE CASCADE,
id_user_owner integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE my_coleguis (
id_user integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
id_colegui integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE,
PRIMARY KEY(id_user, id_colegui)
);

CREATE TABLE pictures (
id integer PRIMARY KEY,
file_picture bytea NOT NULL
);

CREATE TABLE aliments (
id integer PRIMARY KEY,
name_aliment text NOT NULL,
id_picture integer REFERENCES pictures(id) ON UPDATE CASCADE,
food_group integer NOT NULL REFERENCES food_groups(id) ON UPDATE CASCADE,
quantity_base numeric NOT NULL DEFAULT 100,
unit integer NOT NULL REFERENCES units(id) ON UPDATE CASCADE DEFAULT 1,
agua_g numeric DEFAULT 0,
energia_kcal numeric DEFAULT 0,
energia_kj numeric DEFAULT 0,
proteinas_g numeric DEFAULT 0,
grasa_total_g numeric DEFAULT 0,
hidratos_total_g numeric DEFAULT 0,
azucares_g numeric DEFAULT 0,
almidon_g numeric DEFAULT 0,
fibra_dietetica_g numeric DEFAULT 0,
grasa_saturada_g numeric DEFAULT 0,
grasa_monoinsaturada_g numeric DEFAULT 0,
grasa_poliinsaturada_g numeric DEFAULT 0,
colesterol_mg numeric DEFAULT 0,
alcohol_g numeric DEFAULT 0,
calcio_mg numeric DEFAULT 0,
hierro_mg numeric DEFAULT 0,
yodo_g numeric DEFAULT 0,
magnesio_mg numeric DEFAULT 0,
cinc_mg numeric DEFAULT 0,
sodio_mg numeric DEFAULT 0,
potasio_mg numeric DEFAULT 0,
fosforo_mg numeric DEFAULT 0,
selenio_g numeric DEFAULT 0,
tiamina_o_vit_b1_mg numeric DEFAULT 0,
riboflavina_o_vit_b2_mg numeric DEFAULT 0,
eq_niacina_mg numeric DEFAULT 0,
vit_b6_mg numeric DEFAULT 0,
acido_folico_g numeric DEFAULT 0,
vit_b12_g numeric DEFAULT 0,
vit_c_mg numeric DEFAULT 0,
vit_a_eq_retinol_g numeric DEFAULT 0,
retinol_g numeric DEFAULT 0,
carotenos_g numeric DEFAULT 0,
vit_d_g numeric DEFAULT 0,
vit_e_mg numeric DEFAULT 0,
c12_0_g numeric DEFAULT 0,
c14_0_g numeric DEFAULT 0,
c16_0_g numeric DEFAULT 0,
c18_0_g numeric DEFAULT 0,
c18_1_g numeric DEFAULT 0,
c18_2_g numeric DEFAULT 0,
c18_3_g numeric DEFAULT 0,
c20_4_g numeric DEFAULT 0,
c20_5_g numeric DEFAULT 0,
c22_6_g numeric DEFAULT 0,
acidos_grasos_cis_g numeric DEFAULT 0,
acidos_grasos_trans_g numeric DEFAULT 0
);

CREATE TABLE dishes (
id integer PRIMARY KEY,
name_dish text NOT NULL,
id_picture integer REFERENCES pictures(id) ON UPDATE CASCADE,
duration interval NOT NULL,
video text,
recipe text,
posible_position integer NOT NULL REFERENCES positions(id) ON UPDATE CASCADE,
diners integer NOT NULL DEFAULT 1,
id_user_owner integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE,
id_base_dish integer NOT NULL REFERENCES dishes(id) ON UPDATE CASCADE
);

CREATE TABLE ingredients (
id_aliment integer NOT NULL REFERENCES aliments(id) ON UPDATE CASCADE,
id_dish integer NOT NULL REFERENCES dishes(id) ON UPDATE CASCADE ON DELETE CASCADE,
quantity numeric NOT NULL,
unit int NOT NULL REFERENCES units(id) ON UPDATE CASCADE,
main_ingredient boolean NOT NULL DEFAULT FALSE,
PRIMARY KEY(id_aliment, id_dish)
);

CREATE TABLE meals (
id integer PRIMARY KEY,
id_foodup integer NOT NULL REFERENCES foodups(id) ON UPDATE CASCADE ON DELETE CASCADE,
date_day timestamp NOT NULL,
position_of_day integer NOT NULL REFERENCES positions(id) ON UPDATE CASCADE
);

CREATE TABLE relation_foodups_users (
id_foodup integer NOT NULL REFERENCES foodups(id) ON UPDATE CASCADE ON DELETE CASCADE,
id_user integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE,
PRIMARY KEY(id_foodup, id_user)
);

CREATE TABLE relation_dishes_meals (
foodup_group_id integer,
id_dish integer NOT NULL REFERENCES dishes(id) ON UPDATE CASCADE,
id_meal integer NOT NULL REFERENCES meals(id) ON UPDATE CASCADE ON DELETE CASCADE,
dish_position integer NOT NULL REFERENCES positions(id) ON UPDATE CASCADE
);

CREATE TABLE users_restriction (
id_user integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
id_aliment integer NOT NULL DEFAULT -1 REFERENCES aliments(id) ON UPDATE CASCADE
);

CREATE TABLE user_pictures (
id_user integer NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
id_dish integer REFERENCES dishes(id) ON UPDATE CASCADE,
picture_file bytea NOT NULL,
PRIMARY KEY(id_user, id_dish)
);

CREATE SEQUENCE users_seq;
CREATE SEQUENCE foodups_seq;
CREATE SEQUENCE dishes_seq;
CREATE SEQUENCE pictures_seq;
CREATE SEQUENCE aliments_seq;
CREATE SEQUENCE meals_seq;