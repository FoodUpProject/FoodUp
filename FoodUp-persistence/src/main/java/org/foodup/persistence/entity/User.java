package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;

public class User {
	
	private Integer id;
	private String name;
	private String mail;
	private Integer quantity;
	private List<Aliment> allergies;
	private Integer intolerance;
	private List<User> coleguis;
	private List<FoodUp> foodUps;
	private Long lastUpdate;
	private byte[] photoFile;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public List<Aliment> getAllergies() {
		return allergies;
	}
	public void setAllergies(List<Aliment> allergies) {
		this.allergies = allergies;
	}
	public Integer getIntolerance() {
		return intolerance;
	}
	public void setIntolerance(Integer intolerance) {
		this.intolerance = intolerance;
	}
	public List<User> getColeguis() {
		return coleguis;
	}
	public void setColeguis(List<User> coleguis) {
		this.coleguis = coleguis;
	}
	public List<FoodUp> getFoodUps() {
		return foodUps;
	}
	public void setFoodUps(List<FoodUp> foodUps) {
		this.foodUps = foodUps;
	}
	public Long getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public byte[] getPhotoFile() {
		return photoFile;
	}
	public void setPhotoFile(byte[] photoFile) {
		this.photoFile = photoFile;
	}
}
