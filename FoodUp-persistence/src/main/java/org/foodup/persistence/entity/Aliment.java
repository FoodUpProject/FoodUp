package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Aliment {

	private Integer id;
	private Integer idPicture;
	private String name;
	private Integer foodGroup;
	
	public Aliment() {
		
	}
	
	public Aliment(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	public Aliment(Integer id, String name, int foodGroup) {
		this.id = id;
		this.name = name;
		this.foodGroup=foodGroup;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdPicture() {
		return idPicture;
	}

	public void setIdPicture(Integer idPicture) {
		this.idPicture = idPicture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFoodGroup() {
		return foodGroup;
	}

	public void setFoodGroup(Integer foodGroup) {
		this.foodGroup = foodGroup;
	}
	
}
