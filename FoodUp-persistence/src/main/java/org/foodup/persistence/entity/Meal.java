package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;

public class Meal implements Comparable<Meal>{
	
	private int id;
	private Long day;
	private int positionOfDay;
	private List<Dish> dishes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getDay() {
		return day;
	}

	public void setDay(Long day) {
		this.day = day;
	}

	public int getPositionOfDay() {
		return positionOfDay;
	}

	public void setPositionOfDay(int positionOfDay) {
		this.positionOfDay = positionOfDay;
	}

	public List<Dish> getDishes() {
		return dishes;
	}

	public void setDishes(List<Dish> dishes) {
		this.dishes = dishes;
	}

	@Override
	public int compareTo(Meal meal) {
		return Long.compare(this.day, meal.getDay());
	}



}