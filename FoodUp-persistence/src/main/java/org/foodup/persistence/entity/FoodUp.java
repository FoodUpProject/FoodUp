package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;

public class FoodUp {
	
	private Integer id;
	private String name;
	private List<User> diners;
	private Integer color;
	private Long iniDate;
	private Long endDate;
	private Integer daysOfMenu;
	private Integer foodUpType;
	private Integer sameFoodUp;
	private Integer followingFoodUp;
	private Integer idUserOwner;
	private List<Meal> meals;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getDiners() {
		return diners;
	}
	public void setDiners(List<User> diners) {
		this.diners = diners;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}
	public Long getIniDate() {
		return iniDate;
	}
	public void setIniDate(Long iniDate) {
		this.iniDate = iniDate;
	}
	public Long getEndDate() {
		return endDate;
	}
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}
	public Integer getDaysOfMenu() {
		return daysOfMenu;
	}
	public void setDaysOfMenu(Integer daysOfMenu) {
		this.daysOfMenu = daysOfMenu;
	}
	public Integer getFoodUpType() {
		return foodUpType;
	}
	public void setFoodUpType(Integer foodUpType) {
		this.foodUpType = foodUpType;
	}
	public Integer getSameFoodUp() {
		return sameFoodUp;
	}
	public void setSameFoodUp(Integer sameFoodUp) {
		this.sameFoodUp = sameFoodUp;
	}
	public Integer getFollowingFoodUp() {
		return followingFoodUp;
	}
	public void setFollowingFoodUp(Integer followingFoodUp) {
		this.followingFoodUp = followingFoodUp;
	}
	public Integer getIdUserOwner() {
		return idUserOwner;
	}
	public void setIdUserOwner(Integer idUserOwner) {
		this.idUserOwner = idUserOwner;
	}
	public List<Meal> getMeals() {
		return meals;
	}
	public void setMeals(List<Meal> meals) {
		this.meals = meals;
	}
	
}
