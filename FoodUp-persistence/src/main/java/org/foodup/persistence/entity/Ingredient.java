package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Ingredient {
	
	private Aliment aliment;
	private Float quantity;
	private Integer unit;
	private Boolean mainIngredient;
	
	public Aliment getAliment() {
		return aliment;
	}
	public void setAliment(Aliment aliment) {
		this.aliment = aliment;
	}
	public Float getQuantity() {
		return quantity;
	}
	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}
	public Integer getUnit() {
		return unit;
	}
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
	public Boolean getMainIngredient() {
		return mainIngredient;
	}
	public void setMainIngredient(Boolean mainIngredient) {
		this.mainIngredient = mainIngredient;
	}
}
