package org.foodup.persistence.entity;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;

public class Dish {

	private Integer id;
	private String name;
	private Integer idPicture;
	private byte[] filePicture;
	private Integer duration;
	private String url;
	private String recipe;
	private Integer posiblePosition;
	private Integer diners;
	private Integer idUserOwner;
	private Integer idBaseDish;
	private List<Ingredient> ingredients;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIdPicture() {
		return idPicture;
	}
	public void setIdPicture(Integer idPicture) {
		this.idPicture = idPicture;
	}
	public byte[] getFilePicture() {
		return filePicture;
	}
	public void setFilePicture(byte[] filePicture) {
		this.filePicture = filePicture;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRecipe() {
		return recipe;
	}
	public void setRecipe(String recipe) {
		this.recipe = recipe;
	}
	public Integer getPosiblePosition() {
		return posiblePosition;
	}
	public void setPosiblePosition(Integer posiblePosition) {
		this.posiblePosition = posiblePosition;
	}
	public Integer getDiners() {
		return diners;
	}
	public void setDiners(Integer diners) {
		this.diners = diners;
	}
	public Integer getIdUserOwner() {
		return idUserOwner;
	}
	public void setIdUserOwner(Integer idUserOwner) {
		this.idUserOwner = idUserOwner;
	}
	public Integer getIdBaseDish() {
		return idBaseDish;
	}
	public void setIdBaseDish(Integer idBaseDish) {
		this.idBaseDish = idBaseDish;
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

}