package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

class BaseDao {
	
	public void setNullableInteger(PreparedStatement ps, int columnPosition, Integer value ) throws SQLException {
		if (value != null) {
			ps.setInt(columnPosition, value);
		} else {
			ps.setNull(columnPosition, Types.INTEGER);
		}
	}

	public Integer getNullableInteger(ResultSet rs, String columnPosition ) throws SQLException {
		int value = rs.getInt(columnPosition);
	
		return rs.wasNull() ? null : value;
	}
	
	public void setNullableBoolean(PreparedStatement ps, int columnPosition, Boolean value ) throws SQLException {
		if (value != null) {
			ps.setBoolean(columnPosition, value);
		} else {
			ps.setNull(columnPosition, Types.BOOLEAN);
		}
	}

	public Boolean getNullableBoolean(ResultSet rs, String columnPosition ) throws SQLException {
		boolean value = rs.getBoolean(columnPosition);
	
		return rs.wasNull() ? null : value;
	}
	
	
	public void setNullableTimestamp(PreparedStatement ps, int columnPosition, Long value ) throws SQLException {
		if (value != null) {
			ps.setTimestamp(columnPosition, new Timestamp(value));
		} else {
			ps.setNull(columnPosition, Types.TIMESTAMP);
		}
	}

	public Long getNullableTimestamp(ResultSet rs, String columnPosition) throws SQLException {
		Timestamp value = rs.getTimestamp(columnPosition);
	
		return rs.wasNull() ? null : value.getTime() ;
	}
}
