package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.Meal;

class MealDao extends BaseDao {

	public int getNextSequence(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT nextval('meals_seq')");

			rs = ps.executeQuery();

			return rs.next() ? rs.getInt(1) : -1;

		} finally {
			DBUtils.closeResources(rs, ps);
		}

	}

	public List<Meal> getMeals(Connection con, int idFoodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(
					"SELECT id, date_day, position_of_day FROM meals WHERE id_foodup = ? ORDER BY date_day, position_of_day");
			ps.setInt(1, idFoodUp);
			rs = ps.executeQuery();
			
			List<Meal> meals = new ArrayList<>();
			Meal meal;

			while (rs.next()) {
				meal = new Meal();
				meal.setId(rs.getInt("id"));
				meal.setDay(rs.getTimestamp("date_day").getTime());
				meal.setPositionOfDay(rs.getInt("position_of_day"));
				meals.add(meal);
			}

			return meals;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	/**
	 * Insert meal
	 * 
	 * @param meal
	 * @param idFoodUp
	 * @return success boolean
	 * @throws SQLException 
	 */
	public boolean insertMeal(Connection con, Meal meal, int idFoodUp) throws SQLException {
		PreparedStatement ps = null;
		int result = 0;

		try {
			ps = con.prepareStatement("INSERT INTO meals VALUES (?,?,?,?);");
			ps.setInt(1, meal.getId());
			ps.setInt(2, idFoodUp);
			ps.setTimestamp(3, new Timestamp(meal.getDay()));
			ps.setInt(4, meal.getPositionOfDay());
			result = ps.executeUpdate();

		}finally {
			DBUtils.closeResources(ps);
		}

		return result == 1;
	}

	/**
	 * Insert dish in meal
	 * 
	 * @param idMeal
	 * @param idDish
	 * @param position
	 * @return success boolean
	 * @throws SQLException 
	 */
	public int[] insertDishesInMeal(Connection con, Meal meal) throws SQLException {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("INSERT INTO relation_dishes_meals (id_dish, id_meal, dish_position) VALUES (?,?,?)");
			
			int count = 1;
			for (Dish dish : meal.getDishes()) {
				ps.setInt(1, dish.getId());
				ps.setInt(2, meal.getId());
				ps.setInt(3, count);
				ps.addBatch();
				count++;
			}

			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "SET INSERTA MEAL CON ID "+ meal.getId());
			return ps.executeBatch();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	/**
	 * Delete dish in meal
	 * 
	 * @param idMeal
	 * @param idDish
	 * @return
	 * @throws SQLException 
	 */
	public int deleteDishInMeal(Connection con, int idMeal, int idDish) throws SQLException {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("DELETE FROM relation_dishes_meals WHERE id_dish = ? AND id_meal = ?");
			ps.setInt(1, idDish);
			ps.setInt(2, idMeal);
			
			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	public int updateDishInMeal(Connection con, Integer idMeal, Integer idDish, Integer newIdDish) throws SQLException {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("UPDATE relation_dishes_meals SET id_dish = ? WHERE id_meal = ? AND id_dish = ?");
			ps.setInt(1, newIdDish);
			ps.setInt(2, idMeal);
			ps.setInt(3, idDish);
			
			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

}