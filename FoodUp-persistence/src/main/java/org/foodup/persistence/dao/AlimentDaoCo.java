package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 *         This is free software, licensed under the GNU General Public License
 *         v3. See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;

public class AlimentDaoCo {

	public List<Aliment> getAliments() {

		Connection con = null;
		List<Aliment> aliments = null;
		
		try {
			
			con = DBUtils.getConnection();
			
			AlimentDao alimentDao = new AlimentDao();
			
			aliments = alimentDao.getAlimentsAll(con);

		} catch (SQLException e) {
			Logger.getLogger(AlimentDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		
		return aliments;
	}

	public List<Aliment> getAlimentsByName(String name) {

		Connection con = null;
		List<Aliment> aliments = null;
		
		try {
			
			con = DBUtils.getConnection();
			
			AlimentDao alimentDao = new AlimentDao();
			
			aliments = alimentDao.getAlimentsByName(con, name);

		} catch (SQLException e) {
			Logger.getLogger(AlimentDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		
		return aliments;
	}

}
