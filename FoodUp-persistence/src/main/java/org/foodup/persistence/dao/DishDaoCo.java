package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.Ingredient;

public class DishDaoCo {
	
	public List<Dish> getAllDishes(int idUser) {

		Connection con = null;
		List<Dish> dishes = null;
		
		try {
			
			con = DBUtils.getConnection();
			
			DishDao daoDish = new DishDao();
			dishes = daoDish.getListDishes(con, idUser);
			
			if (dishes != null && dishes.size() > 0) {
				IngredientDao daoIngredient = new IngredientDao();
				for (Dish dish: dishes) {
					dish.setIngredients(daoIngredient.getIngredients(con, dish.getId()));
				}
				
			}
			

		} catch (SQLException e) {
			DBUtils.rollbackConnection(con);
			Logger.getLogger(DishDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		
		return dishes;
	}
	
	public boolean createDish(int idUser, Dish dish) {

		Connection con = null;

		try {
			
			// cogemos una conexion y quitamos autoCommit
			con = DBUtils.getConnection();
			con.setAutoCommit(false);
			
			byte[] file = dish.getFilePicture();
			
			if (file != null) {
				//tratamos la imagen
				PictureDao daoImage = new PictureDao();
				int idImage = daoImage.getNextSequence(con);
				daoImage.insertImage(con, idImage, file);
				dish.setIdPicture(idImage);
				dish.setFilePicture(null);
			}
			
			//tratamos el plato
			DishDao daoDish = new DishDao();
			
			int idDish = daoDish.getNextSequence(con);
			dish.setId(idDish);
			dish.setIdUserOwner(idUser);
			
			daoDish.insertDish(con, dish);
			
			//tratamos los ingredientes
			IngredientDao daoIngredient = new IngredientDao();
			daoIngredient.insertIngredients(con, idDish, dish.getIngredients());
				
			con.commit();
			return true;

		} catch (SQLException e) {
			DBUtils.rollbackConnection(con);
			Logger.getLogger(DishDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		
		return false;
	}
	
	
	public boolean updateDish(int idUser, Dish dish) {
		
		Connection con = null;
		
		try  {
			
			// cogemos una conexion y quitamos autoCommit
			con = DBUtils.getConnection();
			con.setAutoCommit(false);
			
			byte[] file = dish.getFilePicture();
			
			if (file != null) {
				//tratamos la imagen
				PictureDao daoImage = new PictureDao();
				int idImage = daoImage.getNextSequence(con);
				daoImage.insertImage(con, idImage, file);
				dish.setIdPicture(idImage);
				dish.setFilePicture(null);
			}
			
			DishDao daoDish = new DishDao();
			daoDish.updateDish(con, dish);
			
			List<Ingredient> ingredients = dish.getIngredients();
			
			if (ingredients != null && !ingredients.isEmpty()) {
				int idDish = dish.getId();
				
				IngredientDao daoIngredient = new IngredientDao();
				
				daoIngredient.deleteIngredients(con, idDish);
				daoIngredient.insertIngredients(con, idDish, ingredients);
				
				con.commit();
				return true;
			}
			
		} catch (SQLException e) {
			Logger.getLogger(DishDaoCo.class.getName()).log(Level.SEVERE, null, e);
			DBUtils.rollbackConnection(con);
		} finally {
			DBUtils.closeResources(con);
		}
		
		return false;
	}

	public boolean deleteDish(Integer idUser, Integer idDish) {
		
		Connection con = null;

		try {
			con = DBUtils.getConnection();
			
			DishDao dishDao = new DishDao();
			
			return dishDao.deleteDish(con, idUser, idDish) != 0;
			
		} catch (SQLException e) {
			Logger.getLogger(DishDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		return false;
	}
}
