package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;

public class PictureDaoCo {

	public byte[] getImageBytesById(int id) {

		Connection con = null;
		byte[] imageByte = null;

		try {
			con = DBUtils.getConnection();
			PictureDao daoImage = new PictureDao();

			imageByte = daoImage.getImageByte(con, id);

		} catch (SQLException e) {
			Logger.getLogger(PictureDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return imageByte;
	}

	public int insertImage(byte[] file) {

		Connection con = null;
		int result = -1;

		try {
			con = DBUtils.getConnection();
			PictureDao daoImage = new PictureDao();

			int id = daoImage.getNextSequence(con);

			if (id != -1) {
				result = (daoImage.insertImage(con, id, file) > 0) ? id : -1;
			}

		} catch (SQLException e) {
			Logger.getLogger(PictureDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return result;
	}
}
