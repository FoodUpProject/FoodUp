package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.FoodUp;
import org.foodup.persistence.entity.Meal;
import org.foodup.persistence.entity.User;

public class UserDaoCo {

	/**
	 * Get user info
	 * 
	 * @param id
	 *            user
	 * @return User or null if something went wrong
	 */
	public User getUser(int id) {

		Connection con = null;
		try {
			con = DBUtils.getConnection();
			User user = null;

			UserDao userDao = new UserDao();
			user = userDao.getUser(con, id);

			if (user != null) {
				
				// set user friends
				user.setColeguis(userDao.getColeguis(con, id));

				// set user allergies
				user.setAllergies(userDao.getUserAllergies(con, id));

				return user;
			}

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDao.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return null;
	}

	/**
	 * Get user info
	 * 
	 * @param id
	 *            user
	 * @return User or null if something went wrong
	 */
	public User getUserAllInfo(int id) {

		Connection con = null;
		try {
			con = DBUtils.getConnection();
			User user = null;

			UserDao userDao = new UserDao();
			user = userDao.getUser(con, id);

			if (user != null) {

				FoodUpDao foodUpDao = new FoodUpDao();
				MealDao mealDao = new MealDao();
				DishDao dishDao = new DishDao();
				IngredientDao ingredientDao = new IngredientDao();
				
				List<FoodUp> foodUps = foodUpDao.getFoodUpsByUser(con, id);

				for (FoodUp foodUp: foodUps) {
					
					int idFoodUp = foodUp.getId();
					
					List<User> usersOnFoodUp = foodUpDao.getUsersOnFoodUp(con, idFoodUp);
					
					for (User userOnFoodUp : usersOnFoodUp) {
						userOnFoodUp.setAllergies(userDao.getUserAllergies(con, userOnFoodUp.getId()));
					}
					
					foodUp.setDiners(usersOnFoodUp);
					
					List<Meal> meals = mealDao.getMeals(con, idFoodUp);
					
					for (Meal meal: meals) {
						
						List<Dish> dishes = dishDao.getDishesByMeal(con, meal.getId());
						
						for (Dish dish : dishes) {
							dish.setIngredients(ingredientDao.getIngredients(con, dish.getId()));
						}
						
						meal.setDishes(dishes);
					}
					
					foodUp.setMeals(meals);
					
				}
				
				user.setFoodUps(foodUps);
				
				// set user friends
				user.setColeguis(userDao.getColeguis(con, id));

				// set user allergies
				user.setAllergies(userDao.getUserAllergies(con, id));

				return user;
			}

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDao.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return null;
	}
	

	public String getPassword(int id) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			return userDao.getPasswordById(con, id);

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return null;
	}
	

	public int getUserId(String mail, String password) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			return userDao.getUserId(con, mail, password);

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return -1;
	}
	

	public byte[] getPhotoUser(int idUser) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			return userDao.getUserPhoto(con, idUser);

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return null;
	}


	public User getUserByMail(String mail) {

		Connection con = null;
		try {
			con = DBUtils.getConnection();

			UserDao userDao = new UserDao();
			int id = userDao.getUserId(con, mail);
			
			if (id != -1) {
				
				User user = null;
				user = userDao.getUser(con, id);

				if (user != null) {

					// set user allergies
					user.setAllergies(userDao.getUserAllergies(con, id));

					return user;
				}
			}

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDao.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return null;
	}

	public User createUser(User user, String password) {

		Connection con = null;

		try {

			con = DBUtils.getConnection();
			con.setAutoCommit(false);

			UserDao userDao = new UserDao();

			if (user.getMail() != null && userDao.getUserIdByMail(con, user.getMail()) == -1) {

				int idUser = userDao.getNextSequence(con);

				if (idUser > 0) {
					user.setId(idUser);

					if (user.getName() == null || user.getName().isEmpty()) {
						user.setName("Nuevo usuario");
					}
					if (user.getQuantity() == null) {
						user.setQuantity(2);
					}
					user.setLastUpdate((new Date()).getTime());

					int result = userDao.insertUser(con, user, password);

					if (result != 0) {

						List<Aliment> aliments = user.getAllergies();

						if (aliments != null && !aliments.isEmpty()) {
							userDao.insertAllergies(con, idUser, aliments);
						}

						con.commit();
						return user;
					}
				}
			}

		} catch (SQLException e) {
			DBUtils.rollbackConnection(con);
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return null;
	}
	

	public User createUser(String email, String password) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			if (userDao.getUserIdByMail(con, email) == -1) {
				User user = new User();

				user.setId(userDao.getNextSequence(con));
				user.setName("Nuevo Usuario");
				user.setMail(email);
				user.setQuantity(2);
				user.setLastUpdate((new Date()).getTime());

				userDao.insertUser(con, user, password);

				return user;
			}

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return null;
	}
	

	public User addColegui(int idUser, int idColegui) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();
				
			if (userDao.insertColegui(con, idUser, idColegui) != 0) {
				User user = userDao.getUser(con, idColegui);
				
				user.setAllergies(userDao.getUserAllergies(con, idColegui));
				
				return user;
			}

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return null;
	}


	public boolean updateUser(User user) {

		Connection con = null;

		try {

			con = DBUtils.getConnection();
			con.setAutoCommit(false);
			
			int idUser = user.getId();

			if (idUser > 0) {

				UserDao userDao = new UserDao();

				if (user.getName() == null || user.getName().isEmpty()) {
					user.setName("Nuevo usuario");
				}
				
				if (user.getQuantity() == null) {
					user.setQuantity(3);
				}
				
				user.setLastUpdate((new Date()).getTime());

				int result = userDao.updateUserInfo(con, user);

				if (result != 0) {
					
					byte[] photoFile = user.getPhotoFile();
					
					if (photoFile != null && photoFile.length > 0) {
						
						Logger.getLogger(UserDaoCo.class.getName()).log(Level.INFO, "Entra en photofile");
						
						userDao.deleteUserImage(con, idUser);
						userDao.insertImageUser(con, idUser, photoFile);
						user.setPhotoFile(null);
					} else {
						
						Logger.getLogger(UserDaoCo.class.getName()).log(Level.INFO, "No entra en photofile");
					}
					
					userDao.deleteAllergies(con, idUser);

					userDao.insertAllergies(con, idUser, user.getAllergies());
					
					//TODO comprobar results
					
					con.commit();
					return true;
				}
			}

		} catch (SQLException e) {
			DBUtils.rollbackConnection(con);
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return false;
	}
	

	public boolean deleteUser(int idUser) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			return userDao.deleteUser(con, idUser) != 0;

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return false;
	}

	
	public boolean deleteColegi(int idUser, int idColegui) {

		try (Connection con = DBUtils.getConnection()) {

			UserDao userDao = new UserDao();

			return userDao.deleteColegui(con, idUser, idColegui) != 0;

		} catch (SQLException e) {
			Logger.getLogger(UserDaoCo.class.getName()).log(Level.SEVERE, null, e);
		}

		return false;
	}
}
