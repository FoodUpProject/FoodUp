package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;
import org.foodup.persistence.entity.Ingredient;

class IngredientDao extends BaseDao {

	
	public List<Ingredient> getIngredients(Connection con, int id_dish) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(
					"SELECT alim.id, alim.name_aliment, alim.id_picture, alim.food_group, ing.quantity, ing.unit, ing.main_ingredient FROM ingredients AS ing, aliments AS alim WHERE ing.id_aliment = alim.id AND ing.id_dish = ?");

			ps.setInt(1, id_dish);
			
			rs = ps.executeQuery();

			List<Ingredient> dishes = new ArrayList<Ingredient>();
			Aliment aliment;
			Ingredient ingredient;

			while (rs.next()) {
				aliment = new Aliment();
				ingredient = new Ingredient();

				aliment.setId(rs.getInt(1));
				aliment.setName(rs.getString(2));
				aliment.setIdPicture(rs.getInt(3));
				aliment.setFoodGroup(rs.getInt(4));
				ingredient.setAliment(aliment);
				ingredient.setQuantity(rs.getFloat(5));
				ingredient.setUnit(rs.getInt(6));
				ingredient.setMainIngredient(rs.getBoolean(7));
				dishes.add(ingredient);
			}

			return dishes;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	public int[] insertIngredients(Connection con, int idDish, List<Ingredient> ingredients) throws SQLException {

		PreparedStatement ps = null;

		try {

			ps = con.prepareStatement(
					"INSERT INTO ingredients (id_aliment, id_dish, quantity, unit, main_ingredient) VALUES (?, ?, ?, ?, ?)");

			for (Ingredient ingredient : ingredients) {

				ps.setInt(1, ingredient.getAliment().getId());
				ps.setInt(2, idDish);
				ps.setFloat(3, ingredient.getQuantity());
				ps.setInt(4, ingredient.getUnit());
				ps.setBoolean(5, ingredient.getMainIngredient());

				ps.addBatch();
			}

			return ps.executeBatch();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	public int[] updateIngredients(Connection con, int idDish, List<Ingredient> ingredients) throws SQLException {

		PreparedStatement ps = null;

		try {

			ps = con.prepareStatement(
					"UPDATE ingredients SET quantity = ?, unit = ?, main_ingredient = ? FROM ingredients WHERE id_aliment = ? AND id_dish = ?");

			for (Ingredient ingredient : ingredients) {

				int idAliment = ingredient.getAliment().getId();

				ps.setFloat(1, ingredient.getQuantity());
				ps.setInt(2, ingredient.getUnit());
				ps.setBoolean(3, ingredient.getMainIngredient());
				ps.setInt(4, idAliment);
				ps.setInt(5, idDish);

				ps.addBatch();
			}

			return ps.executeBatch();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	public int deleteIngredients(Connection con, int idDish) throws SQLException {

		PreparedStatement ps = null;

		try {
			String sql = "DELETE FROM ingredients WHERE id_dish = ?";

			ps = con.prepareStatement(sql);
			ps.setInt(1, idDish);
			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

}
