package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.FoodUp;
import org.foodup.persistence.entity.User;

class DishDao extends BaseDao {

	// TODO obtener dish por usuario modificado

	// SELECT

	public int getNextSequence(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT nextval('dishes_seq')");

			rs = ps.executeQuery();

			return rs.next() ? rs.getInt(1) : -1;

		} finally {
			DBUtils.closeResources(rs, ps);
		}

	}

	public List<Dish> getListDishes(Connection con, int idUser) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(
					"SELECT id, name_dish, id_picture, duration, video, recipe, posible_position, diners, id_user_owner, id_base_dish FROM dishes WHERE id_user_owner IN (1, ?) ORDER BY name_dish");

			ps.setInt(1, idUser);
			
			rs = ps.executeQuery();

			List<Dish> dishes = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();

				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				dishes.add(dish);
			}

			return dishes;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	public List<Dish> getDishesByMeal(Connection con, int idMeal) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(
					"SELECT id, name_dish, id_picture, duration, video, recipe, posible_position, diners, id_user_owner, id_base_dish FROM dishes INNER JOIN relation_dishes_meals ON id_dish = id WHERE id_meal = ? ORDER BY dish_position");

			ps.setInt(1, idMeal);
			
			rs = ps.executeQuery();

			List<Dish> dishes = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();

				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				dishes.add(dish);
			}

			return dishes;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	
	public List<Dish> getBreakfast(Connection con, FoodUp foodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			boolean isExplorer = foodUp.getIdUserOwner().equals(0);
			StringBuilder usersRestrictions = new StringBuilder();
			
			if (isExplorer) {
				if (foodUp.getDiners() != null) {
					for (User user: foodUp.getDiners()) {
						if (user.getAllergies() != null) {
							for (Aliment aliment: user.getAllergies()) {
								usersRestrictions.append(", ").append(aliment.getId());
							}
						}
					}
				}
				if (usersRestrictions.length() > 0) {
					usersRestrictions.delete(0, 2);
				} else {
					usersRestrictions.append(0);
				}
			} else {
				usersRestrictions.append("SELECT ur.id_aliment FROM users_restriction ur ")
					.append("INNER JOIN relation_foodups_users rfu ON ur.id_user = rfu.id_user ")
					.append("WHERE rfu.id_foodup = ?");
			}
			
			ps = con.prepareStatement(
					"SELECT d.id, d.name_dish, d.id_picture, d.duration, d.video, d.recipe, d.posible_position, d.diners, d.id_user_owner, d.id_base_dish FROM dish_positions p "
					+ "INNER JOIN dishes d ON d.posible_position=p.id "
					+ "WHERE d.id_user_owner IN (1, ?) AND p.id IN (1) AND d.id NOT IN ("
					+ "SELECT DISTINCT id_dish "
					+ "FROM ingredients i "
					+ "INNER JOIN aliments a ON i.id_aliment=a.id "
					+ "INNER JOIN food_groups fg ON a.food_group = fg.id "
					+ "WHERE a.id IN ("
					+ usersRestrictions.toString()
					+ ") OR a.id NOT IN ("
					+ "SELECT fr.allowed_aliments "
					+ "FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND fr.allowed_aliments IS NOT NULL) "
					+ "AND EXISTS ("
					+ "SELECT * FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND "
					+ "(fg.id_parent=fr.forbidden_groups OR fr.forbidden_groups=fg.id)))");

			ps.setInt(1, foodUp.getIdUserOwner());
			if (isExplorer) {
				ps.setInt(2, foodUp.getFoodUpType());
				ps.setInt(3, foodUp.getFoodUpType());
			} else {
				ps.setInt(2, foodUp.getId());
				ps.setInt(3, foodUp.getFoodUpType());
				ps.setInt(4, foodUp.getFoodUpType());
			}
			
			rs = ps.executeQuery();

			List<Dish> breakfast = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				breakfast.add(dish);
			}

			return breakfast;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	
	public List<Dish> getBreakfastDrinks(Connection con, FoodUp foodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			boolean isExplorer = foodUp.getIdUserOwner().equals(0);
			StringBuilder usersRestrictions = new StringBuilder();
			
			if (isExplorer) {
				if (foodUp.getDiners() != null) {
					for (User user: foodUp.getDiners()) {
						if (user.getAllergies() != null) {
							for (Aliment aliment: user.getAllergies()) {
								usersRestrictions.append(", ").append(aliment.getId());
							}
						}
					}
				}
				if (usersRestrictions.length() > 0) {
					usersRestrictions.delete(0, 2);
				} else {
					usersRestrictions.append(0);
				}
			} else {
				usersRestrictions.append("SELECT ur.id_aliment FROM users_restriction ur ")
					.append("INNER JOIN relation_foodups_users rfu ON ur.id_user = rfu.id_user ")
					.append("WHERE rfu.id_foodup = ?");
			}
			
			ps = con.prepareStatement(
					"SELECT d.id, d.name_dish, d.id_picture, d.duration, d.video, d.recipe, d.posible_position, d.diners, d.id_user_owner, d.id_base_dish FROM dish_positions p "
					+ "INNER JOIN dishes d ON d.posible_position=p.id "
					+ "WHERE d.id_user_owner IN (1, ?) AND p.id IN (10) AND d.id NOT IN ("
					+ "SELECT DISTINCT id_dish "
					+ "FROM ingredients i "
					+ "INNER JOIN aliments a ON i.id_aliment=a.id "
					+ "INNER JOIN food_groups fg ON a.food_group = fg.id "
					+ "WHERE a.id IN ("
					+ usersRestrictions.toString()
					+ ") OR a.id NOT IN ("
					+ "SELECT fr.allowed_aliments "
					+ "FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND fr.allowed_aliments IS NOT NULL) "
					+ "AND EXISTS ("
					+ "SELECT * FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND "
					+ "(fg.id_parent=fr.forbidden_groups OR fr.forbidden_groups=fg.id)))");

			ps.setInt(1, foodUp.getIdUserOwner());
			if (isExplorer) {
				ps.setInt(2, foodUp.getFoodUpType());
				ps.setInt(3, foodUp.getFoodUpType());
			} else {
				ps.setInt(2, foodUp.getId());
				ps.setInt(3, foodUp.getFoodUpType());
				ps.setInt(4, foodUp.getFoodUpType());
			}
			
			rs = ps.executeQuery();

			List<Dish> drinks = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				drinks.add(dish);
			}

			return drinks;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	public List<Dish> getFirstCourses(Connection con, FoodUp foodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			boolean isExplorer = foodUp.getIdUserOwner().equals(0);
			StringBuilder usersRestrictions = new StringBuilder();
			
			if (isExplorer) {
				if (foodUp.getDiners() != null) {
					for (User user: foodUp.getDiners()) {
						if (user.getAllergies() != null) {
							for (Aliment aliment: user.getAllergies()) {
								usersRestrictions.append(", ").append(aliment.getId());
							}
						}
					}
				}
				if (usersRestrictions.length() > 0) {
					usersRestrictions.delete(0, 2);
				} else {
					usersRestrictions.append(0);
				}
			} else {
				usersRestrictions.append("SELECT ur.id_aliment FROM users_restriction ur ")
					.append("INNER JOIN relation_foodups_users rfu ON ur.id_user = rfu.id_user ")
					.append("WHERE rfu.id_foodup = ?");
			}

			ps = con.prepareStatement(
					"SELECT d.id, d.name_dish, d.id_picture, d.duration, d.video, d.recipe, d.posible_position, d.diners, d.id_user_owner, d.id_base_dish FROM dish_positions p "
					+ "INNER JOIN dishes d ON d.posible_position=p.id "
					+ "WHERE d.id_user_owner IN (1, ?) AND p.id IN (2,4,5) AND d.id NOT IN ("
					+ "SELECT DISTINCT id_dish "
					+ "FROM ingredients i "
					+ "INNER JOIN aliments a ON i.id_aliment=a.id "
					+ "INNER JOIN food_groups fg ON a.food_group = fg.id "
					+ "WHERE a.id IN ("
					+ usersRestrictions.toString()
					+ ") OR a.id NOT IN ("
					+ "SELECT fr.allowed_aliments "
					+ "FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND fr.allowed_aliments IS NOT NULL) "
					+ "AND EXISTS ("
					+ "SELECT * FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND "
					+ "(fg.id_parent=fr.forbidden_groups OR fr.forbidden_groups=fg.id)))");

			ps.setInt(1, foodUp.getIdUserOwner());
			if (isExplorer) {
				ps.setInt(2, foodUp.getFoodUpType());
				ps.setInt(3, foodUp.getFoodUpType());
			} else {
				ps.setInt(2, foodUp.getId());
				ps.setInt(3, foodUp.getFoodUpType());
				ps.setInt(4, foodUp.getFoodUpType());
			}
			
			rs = ps.executeQuery();

			List<Dish> firsts = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				firsts.add(dish);
			}
			return firsts;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	public List<Dish> getSecondCourses(Connection con, FoodUp foodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			boolean isExplorer = foodUp.getIdUserOwner().equals(0);
			StringBuilder usersRestrictions = new StringBuilder();
			
			if (isExplorer) {
				if (foodUp.getDiners() != null) {
					for (User user: foodUp.getDiners()) {
						if (user.getAllergies() != null) {
							for (Aliment aliment: user.getAllergies()) {
								usersRestrictions.append(", ").append(aliment.getId());
							}
						}
					}
				}
				if (usersRestrictions.length() > 0) {
					usersRestrictions.delete(0, 2);
				} else {
					usersRestrictions.append(0);
				}
			} else {
				usersRestrictions.append("SELECT ur.id_aliment FROM users_restriction ur ")
					.append("INNER JOIN relation_foodups_users rfu ON ur.id_user = rfu.id_user ")
					.append("WHERE rfu.id_foodup = ?");
			}
			
			ps = con.prepareStatement(
					"SELECT d.id, d.name_dish, d.id_picture, d.duration, d.video, d.recipe, d.posible_position, d.diners, d.id_user_owner, d.id_base_dish FROM dish_positions p "
					+ "INNER JOIN dishes d ON d.posible_position=p.id "
					+ "WHERE d.id_user_owner IN (1, ?) AND p.id IN (3,4) AND d.id NOT IN ("
					+ "SELECT DISTINCT id_dish "
					+ "FROM ingredients i "
					+ "INNER JOIN aliments a ON i.id_aliment=a.id "
					+ "INNER JOIN food_groups fg ON a.food_group = fg.id "
					+ "WHERE a.id IN ("
					+ usersRestrictions.toString()
					+ ") OR a.id NOT IN ("
					+ "SELECT fr.allowed_aliments "
					+ "FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND fr.allowed_aliments IS NOT NULL) "
					+ "AND EXISTS ("
					+ "SELECT * FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND "
					+ "(fg.id_parent=fr.forbidden_groups OR fr.forbidden_groups=fg.id)))");

			ps.setInt(1, foodUp.getIdUserOwner());
			if (isExplorer) {
				ps.setInt(2, foodUp.getFoodUpType());
				ps.setInt(3, foodUp.getFoodUpType());
			} else {
				ps.setInt(2, foodUp.getId());
				ps.setInt(3, foodUp.getFoodUpType());
				ps.setInt(4, foodUp.getFoodUpType());
			}
			
			rs = ps.executeQuery();

			List<Dish> seconds = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				seconds.add(dish);
			}

			return seconds;	

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	public List<Dish> getDesserts(Connection con, FoodUp foodUp) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			boolean isExplorer = foodUp.getIdUserOwner().equals(0);
			StringBuilder usersRestrictions = new StringBuilder();
			
			if (isExplorer) {
				if (foodUp.getDiners() != null) {
					for (User user: foodUp.getDiners()) {
						if (user.getAllergies() != null) {
							for (Aliment aliment: user.getAllergies()) {
								usersRestrictions.append(", ").append(aliment.getId());
							}
						}
					}
				}
				if (usersRestrictions.length() > 0) {
					usersRestrictions.delete(0, 2);
				} else {
					usersRestrictions.append(0);
				}
			} else {
				usersRestrictions.append("SELECT ur.id_aliment FROM users_restriction ur ")
					.append("INNER JOIN relation_foodups_users rfu ON ur.id_user = rfu.id_user ")
					.append("WHERE rfu.id_foodup = ?");
			}
			
			ps = con.prepareStatement(
					"SELECT d.id, d.name_dish, d.id_picture, d.duration, d.video, d.recipe, d.posible_position, d.diners, d.id_user_owner, d.id_base_dish FROM dish_positions p "
					+ "INNER JOIN dishes d ON d.posible_position=p.id "
					+ "WHERE d.id_user_owner IN (1, ?) AND p.id IN (7) AND d.id NOT IN ("
					+ "SELECT DISTINCT id_dish "
					+ "FROM ingredients i "
					+ "INNER JOIN aliments a ON i.id_aliment=a.id "
					+ "INNER JOIN food_groups fg ON a.food_group = fg.id "
					+ "WHERE a.id IN ("
					+ usersRestrictions.toString()
					+ ") OR a.id NOT IN ("
					+ "SELECT fr.allowed_aliments "
					+ "FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND fr.allowed_aliments IS NOT NULL) "
					+ "AND EXISTS ("
					+ "SELECT * FROM foodup_restrictions fr "
					+ "WHERE fr.id_foodup_type=? AND "
					+ "(fg.id_parent=fr.forbidden_groups OR fr.forbidden_groups=fg.id)))");

			ps.setInt(1, foodUp.getIdUserOwner());
			if (isExplorer) {
				ps.setInt(2, foodUp.getFoodUpType());
				ps.setInt(3, foodUp.getFoodUpType());
			} else {
				ps.setInt(2, foodUp.getId());
				ps.setInt(3, foodUp.getFoodUpType());
				ps.setInt(4, foodUp.getFoodUpType());
			}
			
			rs = ps.executeQuery();

			List<Dish> desserts = new ArrayList<Dish>();
			Dish dish;

			while (rs.next()) {
				dish = new Dish();
				dish.setId(rs.getInt("id"));
				dish.setName(rs.getString("name_dish"));
				dish.setIdPicture(getNullableInteger(rs,"id_picture"));
				dish.setDuration(rs.getInt("duration"));
				dish.setUrl(rs.getString("video"));
				dish.setRecipe(rs.getString("recipe"));
				dish.setPosiblePosition(rs.getInt("posible_position"));
				dish.setDiners(rs.getInt("diners"));
				dish.setIdUserOwner(rs.getInt("id_user_owner"));
				dish.setIdBaseDish(getNullableInteger(rs,"id_base_dish"));
				desserts.add(dish);
			}

			return desserts;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	public HashMap<Integer, Integer> getFoodGroup(Connection con, int idDish) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(
					"SELECT a.id, fg.id FROM food_groups fg "
					+ "INNER JOIN aliments a ON fg.id = a.food_group "
					+ "AND fg.id_parent IS NULL OR fg.id = ("
					+ "SELECT fgp.id_parent FROM food_groups fgp WHERE fgp.id = a.food_group) "
					+ "INNER JOIN ingredients i ON a.id = i.id_aliment "
					+ "WHERE main_ingredient = true AND i.id_dish = ?");
			
			ps.setInt(1, idDish);
			rs = ps.executeQuery();

			HashMap<Integer, Integer> used = new HashMap<>();
			
			while (rs.next()) {
				used.put(rs.getInt(1), rs.getInt(2));
			}

			return used;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}

	// inserta un nuevo plato
	public int insertDish(Connection con, Dish dish) throws SQLException {

		PreparedStatement ps = null;

		try {

			ps = con.prepareStatement(
					"INSERT INTO dishes (id, name_dish, id_picture, duration, video, recipe, posible_position, diners, id_user_owner, id_base_dish) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps.setInt(1, dish.getId());
			ps.setString(2, dish.getName());
			setNullableInteger(ps, 3, dish.getIdPicture());
			ps.setInt(4, dish.getDuration());
			ps.setString(5, dish.getUrl());
			ps.setString(6, dish.getRecipe());
			ps.setInt(7, dish.getPosiblePosition());
			ps.setInt(8, dish.getDiners());
			ps.setInt(9, dish.getIdUserOwner());
			setNullableInteger(ps, 10, dish.getIdBaseDish());

			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	// updatea los datos de un plato
	public int updateDish(Connection con, Dish dish) throws SQLException {

		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(
					"UPDATE dishes SET name_dish = ?, id_picture = ?, duration = ?, video = ?, recipe = ?, posible_position = ?, diners = ? WHERE id = ? AND id_user_owner = ?");

			ps.setString(1, dish.getName());
			setNullableInteger(ps, 2, dish.getIdPicture());
			ps.setInt(3, dish.getDuration());
			ps.setString(4, dish.getUrl());
			ps.setString(5, dish.getRecipe());
			ps.setInt(6, dish.getPosiblePosition());
			ps.setInt(7, dish.getDiners());
			ps.setInt(8, dish.getId());
			ps.setInt(9, dish.getIdUserOwner());

			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

	public int deleteDish(Connection con, Integer idUser, Integer idDish) throws SQLException {

		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("DELETE FROM dishes WHERE id = ? AND id_user_owner = ?");
			ps.setInt(1, idDish);
			ps.setInt(2, idUser);
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}

}
