package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;

class AlimentDao extends BaseDao {
	
	public int getNextSequence(Connection con) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT nextval('aliments_seq')");
			
			rs = ps.executeQuery();
			
			return rs.next() ? rs.getInt(1) : -1;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
		
	}
	
	public Aliment getAlimentsById (Connection con, int id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT id, name_aliment, food_group FROM aliments WHERE id = ?");
			
			ps.setInt(1, id);
			
			rs = ps.executeQuery();
			
			Aliment aliment = null;
			
			if (rs.next()) {
				aliment = new Aliment();
				aliment.setId(rs.getInt("id"));
				aliment.setName(rs.getString("name_aliment"));
				aliment.setFoodGroup(rs.getInt("food_group"));
			}
			
			return aliment;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	public List<Aliment> getAlimentsAll(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT id, name_aliment, id_picture, food_group FROM aliments");
			
			rs = ps.executeQuery();
			
			List<Aliment> aliments = new ArrayList<Aliment>();
			Aliment aliment;
			
			while (rs.next()) {
				aliment = new Aliment();
				aliment.setId(rs.getInt("id"));
				aliment.setName(rs.getString("name_aliment"));
				aliment.setIdPicture(rs.getInt("id_picture"));
				aliment.setFoodGroup(rs.getInt("food_group"));
				aliments.add(aliment);
			}
			
			return aliments;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	public List<Aliment> getAlimentsByName (Connection con, String name) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT id, name_aliment, id_picture, food_group FROM aliments WHERE name_aliment ILIKE ?");
			
			ps.setString(1, "%"+name+"%");
			
			rs = ps.executeQuery();
			
			List<Aliment> aliments = new ArrayList<Aliment>();
			Aliment aliment;
			
			while (rs.next()) {
				aliment = new Aliment();
				aliment.setId(rs.getInt("id"));
				aliment.setName(rs.getString("name_aliment"));
				aliment.setIdPicture(rs.getInt("id_picture"));
				aliment.setFoodGroup(rs.getInt("food_group"));
				aliments.add(aliment);
			}
			
			return aliments;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	public int updateAliment(Connection con, Aliment aliment) throws SQLException {

		PreparedStatement ps = null;
		
		try{
			
			ps = con.prepareStatement("UPDATE aliments SET name_aliment = ?, food_group = ? WHERE id = ?");
			
			ps.setString(1, aliment.getName());
			ps.setInt(2, aliment.getFoodGroup());
			ps.setInt(3, aliment.getId());
			
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}
	
	public int insertAliment(Connection con, Aliment aliment) throws SQLException {

		PreparedStatement ps = null;
		
		try{
			
			ps = con.prepareStatement("INSERT INTO aliments(id, name_aliment, food_group) VALUES( ?, ?, ?)");
			
			ps.setInt(1, aliment.getId());
			ps.setString(2, aliment.getName());
			ps.setInt(3, aliment.getFoodGroup());
			
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}
	
}
