package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.FoodGroup;
import org.foodup.persistence.entity.DishPosition;
import org.foodup.persistence.entity.Unit;

public class ConstantsDaoCo {

	public List<FoodGroup> getAllFoodGroups() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement(
					"SELECT id, name_group, id_parent FROM food_groups ORDER BY id");

			rs = ps.executeQuery();

			List<FoodGroup> foodGroups = new ArrayList<FoodGroup>();
			FoodGroup foodGroup;
			
			while (rs.next()) {
				foodGroup = new FoodGroup();
				foodGroup.setId(rs.getInt("id"));
				foodGroup.setName(rs.getString("name_group"));
				int idFoodGroupParent = rs.getInt("id_parent");
				
				if (idFoodGroupParent == 0) {
					foodGroups.add(foodGroup);
				} else {
					FoodGroup temp = new FoodGroup();
					temp.setId(idFoodGroupParent);
					FoodGroup parentFoodGroup = foodGroups.get(foodGroups.indexOf(temp));
					if (parentFoodGroup.getSubGroup() == null) {
						List<FoodGroup> subFoodGroups = new ArrayList<FoodGroup>();
						subFoodGroups.add(foodGroup);
						parentFoodGroup.setSubGroup(subFoodGroups);
					} else {
						parentFoodGroup.getSubGroup().add(foodGroup);
					}
				}
			}

			return foodGroups;

		} catch (SQLException e) {
			Logger.getLogger(ConstantsDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return null;
	}

	public List<Unit> getAllUnits() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement(
					"SELECT id, code_unit, name_unit FROM units");

			rs = ps.executeQuery();

			List<Unit> units = new ArrayList<Unit>();
			Unit unit;
			
			while (rs.next()) {
				unit = new Unit();
				unit.setId(rs.getInt("id"));
				unit.setSymbol(rs.getString("code_unit"));
				unit.setName(rs.getString("name_unit"));
				units.add(unit);
			}

			return units;

		} catch (SQLException e) {
			Logger.getLogger(ConstantsDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return null;
	}

	public List<DishPosition> getAllDishPositions() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT id, name_position FROM dish_positions");

			rs = ps.executeQuery();

			List<DishPosition> positions = new ArrayList<DishPosition>();
			DishPosition position;
			
			while (rs.next()) {
				position = new DishPosition();
				position.setId(rs.getInt("id"));
				position.setName(rs.getString("name_position"));
				positions.add(position);
			}

			return positions;

		} catch (SQLException e) {
			Logger.getLogger(ConstantsDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return null;
	}
	
}
