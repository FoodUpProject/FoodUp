package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;

public class MealDaoCo {

	public boolean deleteDishMeal(Integer idMeal, Integer idDish) {

		Connection con = null;

		try {
			con = DBUtils.getConnection();

			MealDao mealDao = new MealDao();

			return mealDao.deleteDishInMeal(con, idMeal, idDish) != 0;

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		return false;
	}

	public boolean updateDishMeal(Integer idMeal, Integer idDish, Integer newIdDish) {

		Connection con = null;

		try {
			con = DBUtils.getConnection();

			MealDao mealDao = new MealDao();

			return mealDao.updateDishInMeal(con, idMeal, idDish, newIdDish) != 0;

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		return false;
	}
}
