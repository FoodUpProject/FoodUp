package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.FoodUp;
import org.foodup.persistence.entity.User;

class FoodUpDao extends BaseDao {
	

	// SELECTS

	public int getNextSequence(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT nextval('foodup_seq')");

			rs = ps.executeQuery();

			return rs.next() ? rs.getInt(1) : -1;

		} finally {
			DBUtils.closeResources(rs, ps);
		}

	}


	/**
	 * Insert FoodUp
	 * 
	 * @param foodUp
	 * @return success int
	 * @throws SQLException 
	 */
	public int insertFoodUp(Connection con, FoodUp foodUp) throws SQLException {
		PreparedStatement ps = null;

		try {

			ps = con.prepareStatement("INSERT INTO foodups VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			
			ps.setInt(1, foodUp.getId());
			ps.setString(2, foodUp.getName());
			ps.setInt(3, foodUp.getDiners().size());
			ps.setInt(4, foodUp.getColor());
			ps.setTimestamp(5, new Timestamp(foodUp.getIniDate()));
			setNullableTimestamp(ps, 6, foodUp.getEndDate());
			ps.setInt(7, foodUp.getDaysOfMenu());
			ps.setInt(8, foodUp.getFoodUpType());
			setNullableInteger(ps, 9, foodUp.getSameFoodUp());
			setNullableInteger(ps, 10, foodUp.getFollowingFoodUp());
			ps.setInt(11, foodUp.getIdUserOwner());
			
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}


	/**
	 * Insert user in foodUp
	 * 
	 * @param idUser
	 * @param idFoodUp
	 * @param diners
	 * @return success boolean
	 * @throws SQLException 
	 */
	public int insertUserInFoodUp(Connection con, int idFoodUp, int idUser) throws SQLException {

		PreparedStatement ps = null;

		try {
			
			ps = con.prepareStatement("INSERT INTO relation_foodups_users VALUES (?,?)");
			ps.setInt(1, idFoodUp);
			ps.setInt(2, idUser);
			
			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}
	

	/**
	 * Get FoodUps of User
	 * 
	 * @param idUser
	 * @return List<FoodUp> or null if something went wrong
	 * @throws SQLException 
	 */
	public List<FoodUp> getFoodUpsByUser(Connection con, int idUser) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT DISTINCT id, name, color, init_date, end_date, days_of_menu, foodup_type, foodup_group_id, following_foodup_id, id_user_owner FROM foodups INNER JOIN relation_foodups_users ON id = id_foodup WHERE id_user = ? OR id_user_owner = ? ORDER BY id");
			ps.setInt(1, idUser);
			ps.setInt(2, idUser);
			
			rs = ps.executeQuery();
			
			List<FoodUp> foodUps = new ArrayList<>();
			FoodUp foodUp;

			while (rs.next()) {
				foodUp = new FoodUp();
				foodUp.setId(rs.getInt("id"));
				foodUp.setName(rs.getString("name"));
				foodUp.setColor(rs.getInt("color"));
				foodUp.setIniDate(rs.getTimestamp("init_date").getTime());
				foodUp.setEndDate(getNullableTimestamp(rs, "end_date"));
				foodUp.setDaysOfMenu(rs.getInt("days_of_menu"));
				foodUp.setFoodUpType(rs.getInt("foodup_type"));
				foodUp.setSameFoodUp(getNullableInteger(rs, "foodup_group_id"));
				foodUp.setFollowingFoodUp(getNullableInteger(rs, "following_foodup_id"));
				foodUp.setIdUserOwner(rs.getInt("id_user_owner"));
				foodUps.add(foodUp);
			}

			return foodUps;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	/**
	 * Get FoodUp by id
	 * 
	 * @param idFoodUp
	 * @return FoodUp or null if something went wrong
	 * @throws SQLException 
	 */
	public FoodUp getFoodUpsById(Connection con, int idFoodUp) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT name, color, init_date, end_date, days_of_menu, foodup_type, foodup_group_id, following_foodup_id, id_user_owner FROM foodups WHERE id = ? ");
			ps.setInt(1, idFoodUp);
			
			rs = ps.executeQuery();
			FoodUp foodUp = null;

			if (rs.next()) {
				foodUp = new FoodUp();
				foodUp.setId(idFoodUp);
				foodUp.setName(rs.getString("name"));
				foodUp.setColor(rs.getInt("color"));
				foodUp.setIniDate(rs.getTimestamp("init_date").getTime());
				foodUp.setEndDate(getNullableTimestamp(rs, "end_date"));
				foodUp.setDaysOfMenu(rs.getInt("days_of_menu"));
				foodUp.setFoodUpType(rs.getInt("foodup_type"));
				foodUp.setSameFoodUp(getNullableInteger(rs, "foodup_group_id"));
				foodUp.setFollowingFoodUp(getNullableInteger(rs, "following_foodup_id"));
				foodUp.setIdUserOwner(rs.getInt("id_user_owner"));
			}

			return foodUp;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
	
	/**
	 * Get friends of user
	 * 
	 * @param idUser
	 * @return List<User> or null if something went wrong
	 * @throws SQLException 
	 */
	public List<User> getUsersOnFoodUp(Connection con, int idFoodUp) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement(
					"SELECT id, age, name_user, email, last_update, intolerance FROM users INNER JOIN relation_foodups_users ON id = id_user WHERE  id_foodup = ?");
			ps.setInt(1, idFoodUp);
			rs = ps.executeQuery();
			
			List<User> usersFoodUp = new ArrayList<>();
			User user = null;

			while (rs.next()) {
				user = new User();
				// basic info
				user.setId(rs.getInt("id"));
				user.setQuantity(getNullableInteger(rs, "age"));
				user.setName(rs.getString("name_user"));
				user.setMail(rs.getString("email"));
				user.setLastUpdate(rs.getTimestamp("last_update").getTime());
				user.setIntolerance(getNullableInteger(rs, "intolerance"));
				usersFoodUp.add(user);
			}

			return usersFoodUp;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}
public int[][] getMinimumsMaximumOfFoodUp(Connection con, int foodUpType) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ps = con.prepareStatement("SELECT min_portion, max_portion FROM foodup_min_max where id_foodup_type=? ORDER BY id_parent;");
			ps.setInt(1, foodUpType);
			
			rs = ps.executeQuery();
			
			int[][] minmax= new int[2][8];

			for(int i=0;i<8 && rs.next();i++){
				
				minmax[0][i]=rs.getInt("min_portion");
				minmax[1][i]=rs.getInt("max_portion");
			}

			return minmax;

		} finally {
			DBUtils.closeResources(rs, ps);
		}
	}


	/**
	 * Delete FoodUp
	 * 
	 * @param idFoodUp
	 * @return success boolean
	 * @throws SQLException 
	 */
	public int deleteFoodUp(Connection con, int idUser, int idFoodUp) throws SQLException {
		
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("DELETE FROM foodups WHERE id = ? AND id_user_owner = ?");
			ps.setInt(1, idFoodUp);
			ps.setInt(2, idUser);
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}
	
	/**
	 * Delete user in foodUp
	 * 
	 * @param idFoodUp
	 * @return success boolean
	 * @throws SQLException 
	 */
	public int deleteUserInFoodUp(Connection con, int idFoodUp, int idUser) throws SQLException {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("DELETE FROM relation_foodups_users WHERE id_user=? AND id_foodup=?");
			ps.setInt(1, idUser);
			ps.setInt(2, idFoodUp);
			
			return ps.executeUpdate();

		} finally {
			DBUtils.closeResources(ps);
		}
	}

}