package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.foodup.persistence.DBUtils;

class PictureDao extends BaseDao {
	
	// SELECT

	public int getNextSequence(Connection con) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT nextval('pictures_seq')");
			
			rs = ps.executeQuery();
			
			return rs.next() ? rs.getInt(1) : -1;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
		
	}


	public byte[] getImageByte(Connection con, int id) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			
			ps = con.prepareStatement("SELECT file_picture FROM pictures WHERE id = ?");
			
			ps.setInt(1, id);
			
			rs = ps.executeQuery();
			
			return rs.next() ? rs.getBytes(1) : null;
			
		} finally {
			DBUtils.closeResources(rs, ps);
		}
		
	}
	
	
	// INSERT
	
	public int insertImage(Connection con, int id, byte[] file) throws SQLException {
		
		PreparedStatement ps = null;
		
		try{
			
			ps = con.prepareStatement("INSERT INTO pictures(id, file_picture) VALUES( ?, ?)");
			
			ps.setInt(1, id);
			ps.setBytes(2, file);
			
			return ps.executeUpdate();
			
		} finally {
			DBUtils.closeResources(ps);
		}
	}
	
}
