package org.foodup.persistence.dao;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.foodup.persistence.DBUtils;
import org.foodup.persistence.entity.Aliment;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.FoodUp;
import org.foodup.persistence.entity.Ingredient;
import org.foodup.persistence.entity.Meal;
import org.foodup.persistence.entity.User;

public class FoodUpDaoCo {

	private final static int MEAT = 20;
	private final static int CEREAL = 28;
	private final static int FRUIT = 35;
	private final static int EGG = 40;
	private final static int LEGUME = 43;
	private final static int DIARY = 49;
	private final static int FISH = 56;
	private final static int VEGETABLES = 73;
	private final static int BREAKFAST = 1;
	private final static int LUNCH = 2;
	private final static int DINNER = 3;
	private final static Integer SINGLE_DISH = 5;
	private final static int DAYS_OF_WEEK = 7;
	private List<Integer> days = new ArrayList<>();
	private List<Dish> breakfastDrinksTmp = new ArrayList<>();
	private List<Dish> breakfastsTmp = new ArrayList<>();
	private List<Dish> firstsTmp = new ArrayList<>();
	private List<Dish> secondsTmp = new ArrayList<>();
	private List<Dish> dessertsTmp = new ArrayList<>();
	private List<Dish> breakfastDrinks = new ArrayList<>();
	private List<Dish> breakfasts = new ArrayList<>();
	private List<Dish> firsts = new ArrayList<>();
	private List<Dish> seconds = new ArrayList<>();
	private List<Dish> desserts = new ArrayList<>();
	private int[] minimums;
	private int[] maximums;
	private boolean minOrMax;
	private ArrayList<Integer> notAliments = new ArrayList<>();
	private ArrayList<Integer> notGroups = new ArrayList<>();

	public List<FoodUp> getListFoodUpsByUser(int idUser) {

		Connection con = null;
		try {
			con = DBUtils.getConnection();

			FoodUpDao foodUpDao = new FoodUpDao();

			List<FoodUp> foodUps = foodUpDao.getFoodUpsByUser(con, idUser);

			for (FoodUp foodUp : foodUps) {

				List<User> usersOnFoodUp = foodUpDao.getUsersOnFoodUp(con, foodUp.getId());

				foodUp.setDiners(usersOnFoodUp);
			}

			return foodUps;

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDao.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}

		return null;
	}

	public boolean createFoodUp(int idUser, FoodUp foodUp) {

		Connection con = null;

		try {

			con = DBUtils.getConnection();
			con.setAutoCommit(false);

			FoodUpDao foodUpDao = new FoodUpDao();
			int idFoodUp = foodUpDao.getNextSequence(con);

			if (idFoodUp != -1) {

				foodUp.setId(idFoodUp);
				foodUp.setIdUserOwner(idUser);

				foodUpDao.insertFoodUp(con, foodUp);

				for (User user : foodUp.getDiners()) {

					UserDao userDao = new UserDao();

					Integer idNewUser = user.getId();

					// se crean los nuevos usuarios personalizados en el FoodUp
					if (idNewUser == null) {

						idNewUser = userDao.getNextSequence(con);

						if (idNewUser != -1) {
							user.setId(idNewUser);

							if (user.getName() == null || user.getName().isEmpty()) {
								user.setName("Nuevo usuario");
							}

							if (user.getQuantity() == null) {
								user.setQuantity(2);
							}

							user.setLastUpdate((new Date()).getTime());

							int result = userDao.insertUser(con, user, null);

							if (result != 0) {

								byte[] photoFile = user.getPhotoFile();

								if (photoFile != null && photoFile.length > 0) {

									userDao.deleteUserImage(con, idNewUser);
									userDao.insertImageUser(con, idNewUser, photoFile);
									user.setPhotoFile(null);
								}

								List<Aliment> aliments = user.getAllergies();

								if (aliments != null && !aliments.isEmpty()) {
									userDao.insertAllergies(con, idNewUser, aliments);
								}
							}
						}
					}
					// se crea la relación entre el usuario y el foodUp
					foodUpDao.insertUserInFoodUp(con, idFoodUp, idNewUser);

				}
				con.commit();
				return true;
			}

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
			DBUtils.rollbackConnection(con);
		} finally {
			DBUtils.closeResources(con);
		}

		return false;
	}

	public boolean createFoodUpExplorer(FoodUp foodUp) {

		foodUp.setIdUserOwner(0);

		for (User user : foodUp.getDiners()) {
			if (user.getQuantity() == null) {
				user.setQuantity(2);
			}
		}

		return true;
	}

	/**
	 * Creates a menu for specific foodUp. All courses are retrieved from
	 * database with both restrictions applied: foodUp type restrictions and
	 * allergies of its users. Also, we retrieve from database two tables:
	 * minimums and maximums of food group to be eaten during the week. That is
	 * why we calculate meals by weeks. Then the algorithm gets one dish of
	 * specified course, applying or not some restrictions like having the same
	 * food group or main aliments of the course/s before it, depending of its
	 * position during the day. It's marked as used and we recollect the food
	 * group aliment to be marked as used, as well. When minimums are reached,
	 * we use maximums tables to control food groups in order to be, more or
	 * less, equitable.
	 */
	public boolean createMenu(FoodUp foodUp) {

		Connection con = null;

		try {
			con = DBUtils.getConnection();
			con.setAutoCommit(false);

			DishDao dishDao = new DishDao();

			// get owner of foodUp.
			// This is important above of all in case the owner is not designed
			// as a
			// member of the specified foodUp
			int idUserOwner = foodUp.getIdUserOwner();

			// get all breakfast drinks
			breakfastDrinks = dishDao.getBreakfastDrinks(con, foodUp);
			setIngredientsDishes(con, breakfastDrinks);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE,
					"init breakfastDrinks " + breakfastDrinks.size());
			// get all breakfasts
			breakfasts = dishDao.getBreakfast(con, foodUp);
			setIngredientsDishes(con, breakfasts);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "init breakfasts " + breakfasts.size());
			// get all first dishes
			firsts = dishDao.getFirstCourses(con, foodUp);
			setIngredientsDishes(con, firsts);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "init firsts " + firsts.size());
			// get all second courses
			seconds = dishDao.getSecondCourses(con, foodUp);
			setIngredientsDishes(con, seconds);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "init seconds " + seconds.size());
			// get all desserts
			desserts = dishDao.getDesserts(con, foodUp);
			setIngredientsDishes(con, desserts);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "init desserts " + desserts.size());

			FoodUpDao foodUpDao = new FoodUpDao();
			// get minimum and maximum portions
			minimums = foodUpDao.getMinimumsMaximumOfFoodUp(con, foodUp.getFoodUpType())[0];
			maximums = foodUpDao.getMinimumsMaximumOfFoodUp(con, foodUp.getFoodUpType())[1];

			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "minimums:" + Arrays.toString(minimums));
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "maximums:" + Arrays.toString(minimums));

			// get number of days.
			// In case of being indefinite we are going to create the menu for
			// 30 days
			Date initDate = new Date(foodUp.getIniDate());
			int limitDays = 30;
			if (foodUp.getDaysOfMenu() != -1) {
				int days = foodUp.getDaysOfMenu();
				if (days < limitDays) {
					limitDays = days;
				}
			}

			// get number of weeks and number of days of the last week

			// using Math.ceil we will always have at least one week
			int weeks = (int) Math.ceil((float) limitDays / DAYS_OF_WEEK);
			int daysOfLastWeek = limitDays % DAYS_OF_WEEK;
			if (daysOfLastWeek == 0) {
				daysOfLastWeek = 7;
			}
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "limitDays:" + limitDays);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "weeks:" + weeks);
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "daysOfLastWeek:" + daysOfLastWeek);

			List<Meal> meals = new ArrayList<>();
			for (int i = 0; i < weeks; i++) {

				// get days of this (i) week
				int daysToWeek = 0;
				if (i + 1 == weeks) {
					daysToWeek = DAYS_OF_WEEK - daysOfLastWeek;
				}

				// get random day in order to distribute the minimums and
				// maximums during the week
				Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, "daysToWeek:" + daysToWeek);
				initDays(DAYS_OF_WEEK - daysToWeek);

				// for each day of the week
				for (int j = daysToWeek; j < 7; j++) {

					Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "SET MENU DAY");
					// set menu day
					Calendar cal = Calendar.getInstance();
					cal.setTime(initDate);
					cal.add(Calendar.DATE, i * 7 + getRandomDayOfWeek(days));
					Date day = cal.getTime();
					Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "day" + day);

					// clear saved restrictions
					notAliments.clear();
					notGroups.clear();

					Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "SET BREAKFAST");
					MealDao mealDao = new MealDao();
					// set breakfast
					Meal breakfast = new Meal();
					breakfast.setDay(day.getTime());
					breakfast.setPositionOfDay(BREAKFAST);
					List<Dish> dishesBreakfast = new ArrayList<>();
					dishesBreakfast.add(getBreakfastDrink(con, dishDao, breakfastDrinks));
					dishesBreakfast.add(getBreakfast(con, dishDao, breakfasts));
					breakfast.setDishes(dishesBreakfast);
					breakfast.setId(mealDao.getNextSequence(con));

					// set meals to database if user is real
					if (idUserOwner > 0) {
						mealDao.insertMeal(con, breakfast, foodUp.getId());
						mealDao.insertDishesInMeal(con, breakfast);
					}
					meals.add(breakfast);

					Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "SET LUNCH");
					// set lunch
					notGroups.clear();
					Meal lunch = new Meal();
					lunch.setDay(day.getTime());
					lunch.setPositionOfDay(LUNCH);
					List<Dish> dishesLunch = new ArrayList<>();
					dishesLunch.add(getLunchFirst(con, dishDao, firsts));
					if (!dishesLunch.get(0).getPosiblePosition().equals(SINGLE_DISH)) {
						dishesLunch.add(getLunchSecond(con, dishDao, seconds));
					}
					dishesLunch.add(getLunchDessert(con, dishDao, desserts));

					lunch.setDishes(dishesLunch);
					lunch.setId(mealDao.getNextSequence(con));

					// set meals to database if user is real
					if (idUserOwner > 0) {
						mealDao.insertMeal(con, lunch, foodUp.getId());
						mealDao.insertDishesInMeal(con, lunch);
					}
					meals.add(lunch);

					Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "SET DINNER");
					// set dinner
					notGroups.clear();
					Meal dinner = new Meal();
					dinner.setDay(day.getTime());
					dinner.setPositionOfDay(DINNER);
					List<Dish> dishesDinner = new ArrayList<>();
					dishesDinner.add(getDinnerFirst(con, dishDao, firsts));
					if (!dishesDinner.get(0).getPosiblePosition().equals(SINGLE_DISH)) {
						dishesDinner.add(getDinnerSecond(con, dishDao, seconds));
					}
					dishesDinner.add(getDinnerDessert(con, dishDao, desserts));
					dinner.setDishes(dishesDinner);
					dinner.setId(mealDao.getNextSequence(con));

					// set meals to database if user is real
					if (idUserOwner > 0) {
						mealDao.insertMeal(con, dinner, foodUp.getId());
						mealDao.insertDishesInMeal(con, dinner);
					}
					meals.add(dinner);
				}
			}

			// sort meals by day and set them to foodup
			Collections.sort(meals);
			foodUp.setMeals(meals);

			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "Commit");

			con.commit();
			return true;

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			DBUtils.closeResources(con);
		}

		return false;
	}

	/**
	 * Get random day of possible days of week in order to avoid repeated food
	 * group structures in every week
	 * 
	 * @param days
	 *            possible days
	 * @return random day
	 */
	public int getRandomDayOfWeek(List<Integer> days) {
		int position = (int) Math.floor(Math.random() * days.size());
		int selectedDay = days.get(position);
		days.remove(position);
		return selectedDay;
	}

	/**
	 * Set how many days will have this week
	 * 
	 * @param daysOfWeek
	 */
	public void initDays(int daysOfWeek) {
		Integer[] nums = { 0, 1, 2, 3, 4, 5, 6 };
		days.addAll(Arrays.asList(nums));
		if (daysOfWeek != 7) {
			days = days.subList(0, daysOfWeek);
		}
	}

	/**
	 * Get dish without restrictions from desired collection. Save their main
	 * aliments and food groups to avoid repeated dishes and aliments the same
	 * day
	 * 
	 * @param con
	 * @param dishDao
	 * @param list
	 * @param tempList
	 * @return selected dish
	 * @throws SQLException
	 */
	private Dish getSimpleSearch(Connection con, DishDao dishDao, List<Dish> list, List<Dish> tempList)
			throws SQLException {
		int position = (int) Math.floor(Math.random() * list.size());
		Dish selectedDish = list.get(position);
		tempList.add(selectedDish);
		list.remove(position);
		HashMap<Integer, Integer> foodGroup = dishDao.getFoodGroup(con, selectedDish.getId());
		for (Integer key : foodGroup.keySet()) {
			notAliments.add(key);
		}
		for (Integer values : foodGroup.values()) {
			notGroups.add(values);
		}
		for (Integer integer : foodGroup.values()) {
			setPortion(integer, list, tempList);
		}
		return selectedDish;
	}

	/**
	 * Get dish without aliments listed on notAliments from desired course
	 * list.Save their main aliments and food groups to avoid repeated dishes
	 * and aliments the same day. Loops until he founds one. If after 10 times
	 * doesn't have one, calls changeLists(), if after 20 times doesn't have
	 * one, calls simpleSearch()
	 * 
	 * 
	 * @param con
	 * @param dishDao
	 * @param list
	 * @param tempList
	 * @return selected dish
	 * @throws SQLException
	 */
	private Dish getSemiComplexSearch(Connection con, DishDao dishDao, List<Dish> list, List<Dish> tempList)
			throws SQLException {
		boolean searching = true;
		boolean selected = true;
		Dish selectedDish = null;
		int count = 0;
		int down = 0;
		while (searching) {
			count++;
			down++;
			int position = (int) Math.floor(Math.random() * list.size());
			selectedDish = list.get(position);
			for (Ingredient ingredient : selectedDish.getIngredients()) {
				if (ingredient.getMainIngredient()) {
					for (Integer id : notAliments) {
						if (ingredient.getAliment().getId().equals(id)) {
							selected = false;
						}
					}
				}
			}

			if (selected) {
				tempList.add(selectedDish);
				list.remove(position);
				HashMap<Integer, Integer> foodGroup = dishDao.getFoodGroup(con, selectedDish.getId());
				for (Integer key : foodGroup.keySet()) {
					notAliments.add(key);
				}
				for (Integer values : foodGroup.values()) {
					notGroups.add(values);
					setPortion(values, list, tempList);
				}
				searching = false;
			} else if (count == 10) {
				changeLists(list, tempList);
			} else if (down == 20) {
				Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "semiToSimple");
				return getSimpleSearch(con, dishDao, list, tempList);
			}
		}
		return selectedDish;
	}

	/**
	 * Get dish without aliments nor food groups listed on notAliments and
	 * notGroups from desired course list.Save their main aliments and food
	 * groups to avoid repeated dishes and aliments the same day. Loops until he
	 * founds one. If after 10 times doesn't have one, calls changeLists(), if
	 * after 20 times doesn't have one, calls semiComplexSearch()
	 * 
	 * 
	 * @param con
	 * @param dishDao
	 * @param list
	 * @param tempList
	 * @return selected dish
	 * @throws SQLException
	 */
	private Dish getComplexSearch(Connection con, DishDao dishDao, List<Dish> list, List<Dish> tempList)
			throws SQLException {
		boolean searching = true;
		boolean selected = true;
		Dish selectedDish = null;
		int count = 0;
		int down = 0;
		while (searching) {
			count++;
			down++;
			int position = (int) Math.floor(Math.random() * list.size());
			selectedDish = list.get(position);
			for (Ingredient ingredient : selectedDish.getIngredients()) {
				if (ingredient.getMainIngredient()) {
					for (Integer id : notAliments) {
						if (ingredient.getAliment().getId().equals(id)) {
							selected = false;
						}
					}
				}
			}
			for (Ingredient ingredient : selectedDish.getIngredients()) {
				if (ingredient.getMainIngredient()) {
					for (Integer id : notGroups) {
						if (ingredient.getAliment().getFoodGroup().equals(id)) {
							selected = false;
						}
					}
				}
			}

			if (selected) {
				tempList.add(selectedDish);
				list.remove(position);
				HashMap<Integer, Integer> foodGroup = dishDao.getFoodGroup(con, selectedDish.getId());
				for (Integer key : foodGroup.keySet()) {
					notAliments.add(key);
				}
				for (Integer values : foodGroup.values()) {
					notGroups.add(values);
					setPortion(values, list, tempList);
				}
				searching = false;
			} else if (count == 10) {
				changeLists(list, tempList);
			} else if (down == 20) {
				Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.INFO, "complexToSemi");
				return getSemiComplexSearch(con, dishDao, list, tempList);
			}
		}
		return selectedDish;
	}

	/**
	 * Get breakfast drink with simpleSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getBreakfastDrink(Connection con, DishDao dishDao, List<Dish> breakfastDrinks) throws SQLException {
		return getSimpleSearch(con, dishDao, breakfastDrinks, breakfastDrinksTmp);
	}

	/**
	 * Get breakfast with simpleSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getBreakfast(Connection con, DishDao dishDao, List<Dish> breakfasts) throws SQLException {
		return getSimpleSearch(con, dishDao, breakfasts, breakfastsTmp);
	}

	/**
	 * Get main course for lunch without breakfasts aliments with
	 * SemiComplexSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getLunchFirst(Connection con, DishDao dishDao, List<Dish> firsts) throws SQLException {
		return getSemiComplexSearch(con, dishDao, firsts, firstsTmp);
	}

	/**
	 * Get second course for lunch without breakfasts and launch main course
	 * main aliments nor main food group of main course with ComplexSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getLunchSecond(Connection con, DishDao dishDao, List<Dish> seconds) throws SQLException {
		return getComplexSearch(con, dishDao, seconds, secondsTmp);
	}

	/**
	 * Get launch dessert with simpleSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getLunchDessert(Connection con, DishDao dishDao, List<Dish> desserts) throws SQLException {
		return getSimpleSearch(con, dishDao, desserts, dessertsTmp);
	}

	/**
	 * Get main course for dinner without breakfasts and launch main aliments
	 * with SemiComplexSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getDinnerFirst(Connection con, DishDao dishDao, List<Dish> firsts) throws SQLException {
		return getSemiComplexSearch(con, dishDao, firsts, firstsTmp);
	}

	/**
	 * Get second course for dinner without breakfasts, lunch and dinner main
	 * course main aliments nor main food group with ComplexSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getDinnerSecond(Connection con, DishDao dishDao, List<Dish> seconds) throws SQLException {
		return getComplexSearch(con, dishDao, seconds, secondsTmp);
	}

	/**
	 * Get dinner dessert with simpleSearch()
	 * 
	 * @param con
	 * @param dishDao
	 * @param breakfastDrinks
	 * @return
	 * @throws SQLException
	 */
	private Dish getDinnerDessert(Connection con, DishDao dishDao, List<Dish> desserts) throws SQLException {
		return getSimpleSearch(con, dishDao, desserts, dessertsTmp);
	}

	/**
	 * Checks one food group portion of minimums lists. If all minimums are
	 * checked, it check it from maximums list.
	 * 
	 * If user has reached minimum or maximum food group portion (0 or below),
	 * transfers all dishes with main aliments that belong to that food group in
	 * order to not appear later. In case of leaving list with no dishes in it,
	 * resets list.
	 * 
	 * @param foodGroup
	 * @param list
	 * @param tempList
	 */
	private void setPortion(int foodGroup, List<Dish> list, List<Dish> tempList) {
		int[] minMax;
		if (list.size() == 0) {
			changeLists(list, tempList);
			minOrMax = true;
		}

		if (minOrMax) {
			minMax = maximums;
		} else {
			minMax = minimums;
		}

		switch (foodGroup) {
		case MEAT:
			minMax[0] = minMax[0] - 1;
			if (minMax[0] <= 0) {
				clearDishes(MEAT, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case CEREAL:
			minMax[1] = minMax[1] - 1;
			if (minMax[1] <= 0) {
				clearDishes(CEREAL, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case FRUIT:
			minMax[2] = minMax[2] - 1;
			if (minMax[2] <= 0) {
				clearDishes(FRUIT, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case EGG:
			minMax[3] = minMax[3] - 1;
			if (minMax[3] <= 0) {
				clearDishes(EGG, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case LEGUME:
			minMax[4] = minMax[4] - 1;
			if (minMax[4] <= 0) {
				clearDishes(LEGUME, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case DIARY:
			minMax[5] = minMax[5] - 1;
			if (minMax[5] <= 0) {
				clearDishes(DIARY, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case FISH:
			minMax[6] = minMax[6] - 1;
			if (minMax[6] <= 0) {
				clearDishes(FISH, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		case VEGETABLES:
			minMax[7] = minMax[7] - 1;
			if (minMax[7] <= 0) {
				clearDishes(VEGETABLES, list, tempList);
				if (list.size() == 0) {
					changeLists(list, tempList);
				}
			}
			break;
		}
	}

	/**
	 * Reset courses list.
	 * 
	 * @param list
	 * @param tempList
	 */
	private void changeLists(List<Dish> list, List<Dish> tempList) {
		for (Dish dish : tempList) {
			list.add(dish);
		}
		tempList.clear();
	}

	/**
	 * Removes from the list all dishes that have as a main aliment one with the
	 * same food group than foodGroup parameter
	 * 
	 * @param foodGroup
	 * @param list
	 * @param tempList
	 */
	private void clearDishes(int foodGroup, List<Dish> list, List<Dish> tempList) {
		java.util.Iterator<Dish> it = list.iterator();
		parentLoop: while (it.hasNext()) {
			Dish dish = it.next();
			java.util.Iterator<Ingredient> itIng = dish.getIngredients().iterator();
			while (itIng.hasNext()) {
				Ingredient ingredient = itIng.next();
				if (ingredient.getMainIngredient() && ingredient.getAliment().getFoodGroup().equals(foodGroup)) {
					tempList.add(dish);
					it.remove();
					continue parentLoop;
				}
			}
		}
	}

	private void setIngredientsDishes(Connection con, List<Dish> dishes) throws SQLException {
		IngredientDao ingredientDao = new IngredientDao();

		for (Dish dish : dishes) {
			dish.setIngredients(ingredientDao.getIngredients(con, dish.getId()));
		}
	}

	public boolean deleteFoodUp(Integer idUser, Integer idFoodUp) {

		Connection con = null;

		try {
			con = DBUtils.getConnection();

			FoodUpDao foodUpDao = new FoodUpDao();

			foodUpDao.deleteUserInFoodUp(con, idFoodUp, idUser);

			foodUpDao.deleteFoodUp(con, idUser, idFoodUp);

			return true;

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		return false;
	}

	public FoodUp getFoodUp(Integer idFoodUp) {

		Connection con = null;

		try {
			con = DBUtils.getConnection();

			FoodUpDao foodUpDao = new FoodUpDao();

			FoodUp foodUp = foodUpDao.getFoodUpsById(con, idFoodUp);

			if (foodUp != null) {

				UserDao userDao = new UserDao();
				MealDao mealDao = new MealDao();
				DishDao dishDao = new DishDao();

				List<User> usersOnFoodUp = foodUpDao.getUsersOnFoodUp(con, idFoodUp);

				for (User userOnFoodUp : usersOnFoodUp) {
					userOnFoodUp.setAllergies(userDao.getUserAllergies(con, userOnFoodUp.getId()));
				}

				foodUp.setDiners(usersOnFoodUp);

				List<Meal> meals = mealDao.getMeals(con, idFoodUp);

				for (Meal meal : meals) {

					List<Dish> dishes = dishDao.getDishesByMeal(con, meal.getId());

					setIngredientsDishes(con, dishes);

					meal.setDishes(dishes);
				}

				foodUp.setMeals(meals);

				return foodUp;
			}

		} catch (SQLException e) {
			Logger.getLogger(FoodUpDaoCo.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(con);
		}
		return null;
	}

}