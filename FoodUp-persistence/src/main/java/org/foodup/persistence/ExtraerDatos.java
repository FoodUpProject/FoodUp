package org.foodup.persistence;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ExtraerDatos {

	private static final String DIRECTORY_BASE = "resources/datos/alimentos";
	private static final String FILE_SQL = "resources/sql/alimentos.sql";
	private static final String TOKEN_DELIMITER = ", ";

	private ArrayList<String> inserts;

	public void mostrarAlimentos() {

		inserts = new ArrayList<String>();
		listarDirectorio(new File(DIRECTORY_BASE), "");
		crearArchivoSQL();
		
//
//		Collections.sort(categorias);
//
//		for (String file : categorias) {
//			System.out.println(file);
//		}
//		System.out.println("cantidad de arhivos: " + ficheros.size());
	}

	private void listarDirectorio(File directorio, String directorioPadre) {

		File[] files = directorio.listFiles();

		for (int x = 0; x < files.length; x++) {
			String fileName = files[x].getName();
			String actualPath = directorioPadre + "/" + fileName;

			if (files[x].isDirectory()) {
//				categorias.add(actualPath);
				listarDirectorio(files[x], actualPath);
			} else if (actualPath.matches("^.*.csv$")) {
				
				String[] directorios = actualPath.split("/");
				
				
				String sql = obtenerDatosCsv(files[x]);
				if (obtenerDatosCsv(files[x]).length() > 0) {
					String grupo = directorios[directorios.length-2];
					sql = "INSERT INTO aliments VALUES (nextval('aliments_seq'), '"+fileName.substring(0,fileName.length()-4) + "', null, (SELECT id FROM food_groups WHERE name_group = '"+grupo+"'), 100, 1" + sql + ");";
					//System.out.println(sql);
					inserts.add(sql);
				}
			}
		}
	}

	private String obtenerDatosCsv(File file) {
		String datos = "";
		
		ArrayList<String> campo = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String linea;
			while (br.ready()) {
				linea = br.readLine();
				if (!linea.equals("GENERAL") && !linea.equals("MINERALES") && !linea.equals("VITAMINAS")
						&& !linea.equals("ACIDOS_GRASOS")) {
					
					String[] campos= linea.split("\t");
					
					if (!campo.contains(campos[0])) {
						campo.add(campos[0]);
						datos += TOKEN_DELIMITER + campos[1];
						//System.out.println(campos[0]);
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return datos;
	}
	
	private void crearArchivoSQL() {
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_SQL, false))) {
			
			for (String sql: inserts) {
				bw.write(sql + "\n");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {

		ExtraerDatos ex = new ExtraerDatos();
		ex.mostrarAlimentos();
	}
}
