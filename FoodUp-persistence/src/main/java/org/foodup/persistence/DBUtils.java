package org.foodup.persistence;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBUtils {
	
	public static synchronized Connection getConnection() {
		try {
			Context ic = new InitialContext();
			DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/foodupDB");
			return ds.getConnection();
		} catch (NamingException e) {
			Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}

	public static void closeResources(AutoCloseable... resources) {
		for (AutoCloseable autoCloseable : resources) {
			if (autoCloseable != null) {
				try {
					autoCloseable.close();
				} catch (Exception e) {
					Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
	}

	public static void rollbackConnection(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
			}
		}
	}

}
