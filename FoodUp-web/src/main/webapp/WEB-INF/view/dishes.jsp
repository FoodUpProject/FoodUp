<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.lang.reflect.Type"%>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.reflect.TypeToken"%>
<%@ page import="org.foodup.persistence.entity.Dish"%>
<%@ page import="org.foodup.persistence.entity.Aliment"%>
<%@ page import="org.foodup.persistence.entity.Ingredient"%>
<%@ page import="org.foodup.persistence.entity.DishPosition"%>
<%@ page import="org.foodup.persistence.entity.Unit"%>
<%@ page import="org.foodup.persistence.entity.FoodGroup"%>
<!DOCTYPE html>
<head>
<!-- ADD ALL YOUR CSS DEPENDENCIES HERE... -->
<link rel="stylesheet" href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="webjars/bootstrap-select/1.9.4/css/bootstrap-select.min.css" rel="stylesheet">
</head>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Platos</title>
<%
	Gson gson = new Gson();
	Type alimentType = new TypeToken<List<Aliment>>() {}.getType();
	Type dishType = new TypeToken<List<Dish>>() {}.getType();
	Type dishPositionType = new TypeToken<List<DishPosition>>() {}.getType();
	Type unitType = new TypeToken<List<Unit>>() {}.getType();
	Type foodGroupType = new TypeToken<List<FoodGroup>>() {}.getType();
	List<Aliment> aliments = gson.fromJson((String) request.getAttribute("aliments"), alimentType);
	List<Dish> dishes = gson.fromJson((String) request.getAttribute("dishes"), dishType);
	List<DishPosition> positions = gson.fromJson((String) request.getAttribute("dishPositions"), dishPositionType);
	List<Unit> units = gson.fromJson((String) request.getAttribute("units"), unitType);
	List<FoodGroup> foodGroups = gson.fromJson((String) request.getAttribute("foodGroups"), foodGroupType);
%>
<style type="text/css">
	
#createDish{
  overflow:auto;
}
</style>
<script type="text/javascript">

//variables que obtendrá los datos de los alimentos y platos
var aliments = <%=(String) request.getAttribute("aliments")%>;
var dishes = <%=(String) request.getAttribute("dishes")%>;
var dishPositions = <%=(String) request.getAttribute("dishPositions")%>;
var foodGroups = <%=(String) request.getAttribute("foodGroups")%>;
var units = <%=(String) request.getAttribute("units")%>;

//id
var ingredientsToSend = [];

// setea y muestra el modal que muestra los detalles del plato
function showDishDetail(index) {
	var dish = dishes[index];
	//setea los campos
	$("#detail_name").text(dish["name"]);
 	$("#detail_image").attr('src', "pictures?id="+dish["idPicture"]);
 	$("#detail_image").attr('name', dish["name"]);
	$("#detail_duration").text(dish["duration"]);
	$("#detail_position").text(getPositionName(dish["posiblePosition"]));
	$("#detail_diners").text(dish["diners"]);
	if (dish["url"].length == 0) {
		$("#detail_url").text("No hay enlace");
	} else {
		$("#detail_url").text(dish["url"]);
	}
	if (dish["recipe"].length == 0) {
		$("#detail_recipe").text("No hay receta");
	} else {
		$("#detail_recipe").text(dish["recipe"]);
	}
	$("#detail_ingredients").empty();

	for (var i in dish["ingredients"]) {
		var button = $("<input>");
		button.attr('type', "checkBox");
		button.attr('checked', dish["ingredients"][i]["mainIngredient"]);
		button.attr('disabled', "true");
		$("<tr>").append(
				
// 				$("<td>"
//				,{
// 					"class":"text-center"}).append("<img>",{
// 					//"src":"pictures?id="+ingredient["aliment"]["idPicture"],
// 					"alt":ingredient["aliment"]["name"]
// 				}
// 				),
				$("<td>", {
					"class":"text-center",
					"text":dish["ingredients"][i]["aliment"]["name"]
				}),
				$("<td>", {
					"class":"text-center",
					"text":dish["ingredients"][i]["quantity"]
				}),
				$("<td>", {
					"class":"text-center",
					"text":getUnitName(dish["ingredients"][i]["unit"])
				}),
				$("<td>", {
					"class":"text-center"
				}).append(button)).appendTo("#detail_ingredients");
	}
	
	//muestra el modal del detalle del plato
    $('#detail_dish').modal({
        show: true
    });
}

function getPositionName(position) {
	for (var i=0; i < dishPositions.length; i++){
	    if(dishPositions[i]["id"] == position) {
	       return dishPositions[i]["name"];
	    }
	}
}

function getUnitName(unit) {
	for (var i=0; i < units.length; i++){
	    if(units[i]["id"] == unit) {
	       return units[i]["name"];
	    }
	}
}

function valIdIngredient(id) {
	for (var i in ingredientsToSend) {
		if (ingredientsToSend[i]["aliment"]["id"] == id) {
			return false;
		}
	}
	return true;
}

function showNewIngredient() {
	var tabla = $("#showNewsIngredients");
	tabla.empty();
	for (var i = 0; i < ingredientsToSend.length; i++) {
		var button = $("<button>");
		button.addClass("btn btn-primary");
		button.attr('type', "button");
		button.attr('onclick', "deleteIngredient("+i+")");
		button.append("Eliminar");
		$("<tr>").append(
				$("<td>", {
					"class":"text-center", text: ingredientsToSend[i]["aliment"]["name"]}),
				$("<td>", {
					"class":"text-center", text: ingredientsToSend[i]["quantity"]}),
				$("<td>", {
					"class":"text-center", text: getUnitName(ingredientsToSend[i]["unit"])}),
				$("<td>",{
					"class":"text-center"}
				).append($("<input>",{
					"type":"checkbox",
					"checked": ingredientsToSend[i]["mainIngredient"],
					"disabled": true
				})),
				$("<td>").append(button)).appendTo(tabla);
	}
}

function deleteIngredient(index) {
	ingredientsToSend.splice(index,1);
	showNewIngredient();
}

function setIngredientesForm() {
	$("#ingredientsData").val(JSON.stringify(ingredientsToSend));
}

document.addEventListener("DOMContentLoaded", function(event) {
	
	$('#addAliments').on('hidden.bs.modal', function () {

		$('#createDish').hide().show();
	})
	
	//modal que recoge los datos de un nuevo ingrediente
	
	$("#addAliment").on('click', function() {
		var aliment = $('#nameAliment').find("option:selected");
		var id = aliment.val();
		var quantity = $('#quantityIngredient');
		var unit = $('#unitIngredient');
		var mainIngredient = false;
		if ($('#principalIngredient:checked').val()) {
			mainIngredient = true;
		}
		if (!valIdIngredient(id)) {
			alert("Ingrediente repetido");
		}else if (id > 0 && quantity.val() > 0 && unit.val() > 0) {
			ingredientsToSend.push({
				"aliment" : {"id":id,"name":aliment.text()},
				"quantity" : quantity.val(),
				"unit" : unit.val(),
				"mainIngredient" : mainIngredient
			});
			showNewIngredient();
			quantity.val(0);
			unit.val(1);
			$('#principalIngredient').prop('checked', false);
			//todo ocultar modal
		} else {
			alert("Parámetros incorrectos");
		}
	});
	
	
  });

</script>
</head>
<body>

	<div class="container">

		<h2>¡Dishes!</h2>

		
		<br>
		<div class="text-center">
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#createDish">Nuevo plato</button>
		</div>
		<br>
		<br>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Imagen</th>
					<th>Nombre</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<%
					if (dishes.size() > 0) {
						for (int i = 0; i < dishes.size(); i++) {
				%>
				<tr>
					<td><img
						src="pictures?id=<%=dishes.get(i).getIdPicture()%>"
						alt="<%=dishes.get(i).getName()%>" height="75" width="75"></td>
					<td ><%=dishes.get(i).getName()%></td>
					<td class="text-right"><button type="button" class="btn btn-primary" onclick="showDishDetail(<%=i%>)">Ver
							detalles</button></td>
				</tr>
				<%
					}
					}
				%>
			</tbody>
		</table>

		<br>
		<div class="text-center">
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#createDish">Nuevo plato</button>
		</div>

		<div id="detail_dish" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title text-center" id="detail_name"></h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img id="detail_image" height="200" width="250">
						</div>
						<br>
						<dl class="dl-horizontal">
							<dt>Duración (minutos):</dt>
							<dd id="detail_duration"></dd>
							<dt>Posiciones posibles</dt>
							<dd id="detail_position"></dd>
							<dt>Comensales</dt>
							<dd id="detail_diners"></dd>
							<dt>Url de video</dt>
							<dd id="detail_url"></dd>
						</dl>
						<dl class="dl">
							<dt>Receta</dt>
							<dd id="detail_recipe"></dd>
						</dl>
						<h4 class="text-center">Ingredientes</h4>
						<table class="table table-striped">
							<thead>
								<tr>
<!-- 									<th>Imagen</th> -->
									<th class="text-center">Nombre</th>
									<th class="text-center">Cantidad</th>
									<th class="text-center">Unidad</th>
									<th class="text-center">Ingrediente principal</th>
								</tr>
							</thead>
							<tbody id="detail_ingredients">
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->


		<div id="createDish" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">

					<form id="createDishForm" method="post" class="form-horizontal"
						action="platos" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title text-center">Crear un nuevo plato</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-xs-3 control-label">Nombre</label>
								<div class="col-xs-5">
									<input type="text" class="form-control" name="name" required />
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Seleccionar imagen</label>
								<div class="col-xs-5 inputGroupContainer">
									<div class="input-group">
										<input type="file" class="form-control file" name="image"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Tiempo de
									opreparación</label>
								<div class="col-xs-3 inputGroupContainer">
									<div class="input-group">
										<input type="number" class="form-control" name="duration"
											min="0" value="0" required /> <span class="input-group-addon">min.</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Tipo plato</label>
								<div class="col-xs-3 selectContainer">
									<select name="position" class="form-control" required>
										<%
											for (DishPosition position : positions) {
										%>
										<option value="<%=position.getId()%>"><%=position.getName()%></option>
										<%
											}
										%>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Comensales</label>
								<div class="col-xs-3 inputGroupContainer">
									<div class="input-group">
										<input type="number" class="form-control" name="diners"
											min="1" value="1" required />
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Url video</label>
								<div class="col-xs-5">
									<input type="text" class="form-control" name="url" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-3 control-label">Receta</label>
								<div class="col-xs-8">
									<textarea name="recipe" class="form-control" rows="5"></textarea>
								</div>
							</div>

							<button type="button" data-toggle="modal" class="btn btn-primary"
								data-target="#addAliments">Añadir un ingrediente</button>
							<input id="ingredientsData" type="hidden" name="ingredients" value="asd">

							<h4 class="text-center">Ingredientes</h4>
							<table class="table table-striped">
								<thead>
									<tr>
<!-- 										<th>Imagen</th> -->
										<th class="text-center">Ingrediente</th>
										<th class="text-center">Cantidad</th>
										<th class="text-center">Unidad</th>
										<th class="text-center">Ingrediente principal</th>
										<th class="text-center"></th>
									</tr>
								</thead>
								<tbody id="showNewsIngredients">
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-success" onclick="setIngredientesForm()">Guardar</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->


		<!-- 		Modal donde se añaden ingredientes -->

		<div id="addAliments" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						
						<h4 class="modal-title text-center">Añadir un ingrediente</h4>
					</div>
					<div class="modal-body">

						<form class="form-horizontal">

							<div class="form-group">
								<label class="col-xs-4 control-label">Nombre</label>
								<div class="col-xs-5" row-fluid>
									<select id="nameAliment" class="selectpicker" data-show-subtext="true" data-live-search="true">
										<%
											for (Aliment ali : aliments) {
												FoodGroup foodG = new FoodGroup();
												foodG.setId(ali.getFoodGroup());
												if (foodGroups.contains(foodG)) {
													foodG = foodGroups.get(foodGroups.indexOf(foodG));
												} else {
													for (int i = 0; i<foodGroups.size(); i++) {
														if (foodGroups.get(i).getSubGroup() != null && foodGroups.get(i).getSubGroup().contains(foodG)) {
															foodG = foodGroups.get(i);
															break;
														}
													}
												}
												
										%>
										<option data-subtext="<%=foodG.getName()%>" value="<%=ali.getId()%>"><%=ali.getName()%></option>
										<%
											}
										%>
									
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-4 control-label">Cantidad</label>
								<div class="col-xs-3 inputGroupContainer">
									<div class="input-group">
										<input id="quantityIngredient" type="number"
											class="form-control" value="1" step="any" min="0"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-4 control-label">Unidad</label>
								<div class="col-xs-3 selectContainer">
									<select id="unitIngredient" class="form-control">
										<%
											for (Unit unit : units) {
										%>
										<option value="<%=unit.getId()%>"><%=unit.getName()%></option>
										<%
											}
										%>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-xs-4 control-label">Ingrediente
									principal</label>
								<div class="col-sm-3">
									<input id="principalIngredient" type="checkbox"
										class="form-control" />
								</div>
							</div>

						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar modal</button>
						<button type="button" class="btn btn-success" id="addAliment">Añadir ingrediente</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

	</div>

	<!-- ADD ALL YOUR JS DEPENDENCIES HERE... -->
	<script src="webjars/jquery/2.2.3/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="webjars/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
</body>
</html>