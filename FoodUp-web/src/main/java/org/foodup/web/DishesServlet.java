package org.foodup.web;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.foodup.persistence.dao.AlimentDaoCo;
import org.foodup.persistence.dao.ConstantsDaoCo;
import org.foodup.persistence.dao.DishDaoCo;
import org.foodup.persistence.entity.Aliment;
import org.foodup.persistence.entity.Dish;
import org.foodup.persistence.entity.DishPosition;
import org.foodup.persistence.entity.FoodGroup;
import org.foodup.persistence.entity.Ingredient;
import org.foodup.persistence.entity.Unit;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet implementation class Dishes
 */
public class DishesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ConstantsDaoCo daoConstants = new ConstantsDaoCo();
		AlimentDaoCo daoAliment = new AlimentDaoCo();
		DishDaoCo daoDish = new DishDaoCo();
		Gson gson = new Gson();

		List<Aliment> aliments = daoAliment.getAliments();
		List<DishPosition> dishPositions = daoConstants.getAllDishPositions();
		List<FoodGroup> foodGroups = daoConstants.getAllFoodGroups();
		List<Unit> units = daoConstants.getAllUnits();

		request.setAttribute("aliments", gson.toJson(aliments));
		request.setAttribute("dishPositions", gson.toJson(dishPositions));
		request.setAttribute("units", gson.toJson(units));
		request.setAttribute("foodGroups", gson.toJson(foodGroups));

		request.setAttribute("dishes", gson.toJson(daoDish.getAllDishes(1)));

		request.getRequestDispatcher("WEB-INF/view/dishes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recogemos los datos que llegan de la request
		String name = request.getParameter("name");
		Part image = request.getPart("image");
		String duration = request.getParameter("duration");
		String position = request.getParameter("position");
		String diners = request.getParameter("diners");
		String url = request.getParameter("url");
		String recipe = request.getParameter("recipe");
		String ingredients = request.getParameter("ingredients");

		// validamos los datos indispensables para la correcta ejecución
		if (name != null && duration != null && position != null && ingredients != null && diners != null
				&& duration.matches("^[0-9]+$") && position.matches("^[0-9]+$") && diners.matches("^[0-9]+$")) {
			byte[] imageBytes = null;
			if (image != null && image.getSize() > 0) {
				imageBytes = getBytesFromImage(image);
			}

			Gson gson = new Gson();
			Type ingredientType = new TypeToken<List<Ingredient>>() {
			}.getType();
			List<Ingredient> ingredientsObj = gson.fromJson(ingredients, ingredientType);

			Dish dish = new Dish();

			dish.setName(new String (name.getBytes ("iso-8859-1"), "UTF-8"));
			dish.setDuration(Integer.parseInt(duration));
			dish.setUrl(url);
			dish.setRecipe(new String (recipe.getBytes ("iso-8859-1"), "UTF-8"));
			dish.setPosiblePosition(Integer.parseInt(position));
			dish.setDiners(Integer.parseInt(diners));
			dish.setIngredients(ingredientsObj);
			dish.setFilePicture(imageBytes);

			DishDaoCo daoDish = new DishDaoCo();
			if (daoDish.createDish(1, dish)) {
				request.setAttribute("result", "OK");
			} else {

				request.setAttribute("result", "PROBLEM DAOS");
			}

		} else {
			request.setAttribute("result", 
					"nombre '" + name + "'\n" +
							"image '" + image + "'\n" +
							"duration '" + duration + "'\n" +
							"position '" + position + "'\n" +
							"ingredients '" + ingredients + "'\n" +
							"diners '" + diners + "'\n" +
					"PROBLEM INPUTS");
		}

		doGet(request, response);
	}

	private byte[] getBytesFromImage(Part image) throws IOException {
		InputStream inputStream = image.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] tempBytes = new byte[1024];
		while (inputStream.read(tempBytes) != -1) {
			baos.write(tempBytes);
		}
		return baos.toByteArray();
	}

}
