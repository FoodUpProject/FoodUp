package org.foodup.web;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.foodup.persistence.dao.PictureDaoCo;

/**
 * Servlet implementation class PictureServlet
 */
public class PictureServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idPictureString = request.getParameter("id");
		if (idPictureString != null  && idPictureString.matches("^[0-9]+$")) {
			PictureDaoCo daoPicture = new PictureDaoCo();
			byte[] file = daoPicture.getImageBytesById(Integer.parseInt(idPictureString));
			
			if (file != null) {
				OutputStream os = response.getOutputStream();
				response.setContentType("image/jpg");
				os.write(file);
				os.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
