package org.foodup.resources;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.foodup.persistence.dao.AlimentDaoCo;
import org.foodup.persistence.entity.Aliment;
import org.foodup.utils.DataResponse;

@Path("/aliments")
@Produces("application/json")
public class AlimentResource {
	
	@GET
	@PermitAll
    public Response getAllAliments(@QueryParam("name") String name) {
		
		AlimentDaoCo daoAliment = new AlimentDaoCo();
		DataResponse<List<Aliment>> response = new DataResponse<>();
		
		if (name == null) {
			response.setData(daoAliment.getAliments());
		} else {
			response.setData(daoAliment.getAlimentsByName(name));
		}

		return Response.ok(response).build();
	}
	
}
