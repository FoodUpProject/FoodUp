package org.foodup.resources;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.foodup.persistence.dao.UserDaoCo;
import org.foodup.persistence.entity.User;
import org.foodup.utils.DataResponse;
import org.foodup.utils.ValidationUtils;

@Path("/users")
@Produces("application/json")
public class UserResource {

	@POST
	@PermitAll
	public Response createUser(@HeaderParam("Authorization") String auth) {

		DataResponse<User> response = new DataResponse<>();

		if (auth != null) {

			ValidationUtils val = new ValidationUtils();
			// emailAndPassword
			String[] credentials = val.decode(auth);

			// If email or password fail
			if (credentials != null && credentials.length == 2) {

				String token = val.generateHashSha256(credentials[1]);

				UserDaoCo userDao = new UserDaoCo();

				User user = userDao.createUser(credentials[0], token);

				if (user != null) {
					response.setData(user);
					response.setToken(token);

					return Response.ok(response).build();
				} else {
					return Response.status(Status.CONFLICT).build();
				}
			}

		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@GET
	@PermitAll
	@Path("{id}")
	public Response getUser(@PathParam("id") Integer id) {

		UserDaoCo userDao = new UserDaoCo();
		DataResponse<User> response = new DataResponse<>();
		User user;

		user = userDao.getUserAllInfo(id);

		if (user != null) {
			response.setData(user);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	public Response getUser(@Context ContainerRequestContext crc, @QueryParam("email") String email) {

		Integer id = (Integer) crc.getProperty("idUser");
		String token = (String) crc.getProperty("token");

		UserDaoCo userDao = new UserDaoCo();
		DataResponse<User> response = new DataResponse<>();
		User user;

		if (email == null) {
			user = userDao.getUser(id);
		} else {
			user = userDao.getUserByMail(email);
		}

		if (user != null) {
			response.setData(user);
			response.setToken(token);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@PUT
	public Response updateUser(@Context ContainerRequestContext crc, User user) {

		Integer id = (Integer) crc.getProperty("idUser");

		if (user != null && user.getId().equals(id)) {
			UserDaoCo userDao = new UserDaoCo();
			DataResponse<User> response = new DataResponse<>();

			if (userDao.updateUser(user)) {
				response.setData(user);
				return Response.ok(response).build();
			}

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.status(Status.BAD_REQUEST).build();

	}

	@GET
	@PermitAll
	@Path("/photo/{id}")
	@Produces({ "image/jpeg" })
	public Response getAllBoxes(@PathParam("id") Integer id) {

		UserDaoCo daoUser = new UserDaoCo();

		byte[] picture = daoUser.getPhotoUser(id);

		if (picture != null && picture.length > 0) {
			return Response.ok(picture).build();
		} else {
			return Response.status(404).build();
		}

	}
}
