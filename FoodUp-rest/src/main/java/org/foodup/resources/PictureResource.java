package org.foodup.resources;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.foodup.persistence.dao.PictureDaoCo;

@Path("/pictures")
@Produces({"image/jpeg"})
public class PictureResource {
	
	@GET 
	@PermitAll
	@Path("{id}")
    public Response getPicture(@PathParam("id") Integer id) {
		
		PictureDaoCo daoPicture = new PictureDaoCo();
		
		byte[] picture = daoPicture.getImageBytesById(id);
		
		if (picture != null && picture.length>0) {
			return Response.ok(picture).build();
		} else {
			return Response.status(404).build();
		}
		
	}
}
