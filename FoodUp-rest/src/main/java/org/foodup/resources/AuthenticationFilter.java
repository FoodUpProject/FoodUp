package org.foodup.resources;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import javax.annotation.security.PermitAll;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.foodup.persistence.dao.UserDaoCo;
import org.foodup.utils.ValidationUtils;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInnfo;

	@Override
	public void filter(ContainerRequestContext requestContext) {

		boolean permitMethod = resourceInnfo.getResourceMethod().isAnnotationPresent(PermitAll.class);

		if (!permitMethod) {

			// Get the authentification passed in HTTP headers parameters
			String auth = requestContext.getHeaderString("Authorization");

			// If the user does not have the right (does not provide any HTTP
			// Basic Auth)
			if (auth == null) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
			ValidationUtils val = new ValidationUtils();
			// emailAndPassword
			String[] credentials = val.decode(auth);

			// If email or password fail
			if (credentials == null) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
			
			// si accedemos con token
			if (credentials.length == 3 && credentials[0].matches("^[0-9]+$") && credentials[1].matches("^[0-9]+$")) {

				Integer idUser = Integer.parseInt(credentials[0]);
				long time = Long.parseLong(credentials[1]);

				UserDaoCo userDao = new UserDaoCo();
				String encryptedPassword = userDao.getPassword(idUser);

				if (encryptedPassword != null) {
					String generatedTocken = val.generateToken(time, encryptedPassword);

					if (generatedTocken.equals(credentials[2])) {
						requestContext.setProperty("idUser", idUser);
						return;
					}
				}
			// si accedemos con usuario y pass
			} else if (credentials.length == 2) {

				String encryptedPassword = val.generateHashSha256(credentials[1]);

				UserDaoCo userDao = new UserDaoCo();
				Integer idUser = userDao.getUserId(credentials[0], encryptedPassword);

				if (idUser != -1) {
					// token que se debe devolver para 
					requestContext.setProperty("token", encryptedPassword);
					requestContext.setProperty("idUser", idUser);
					return;
				}

			}
			
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

	}

}
