package org.foodup.resources;

import java.util.List;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import javax.annotation.security.PermitAll;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.foodup.persistence.dao.FoodUpDaoCo;
import org.foodup.persistence.dao.MealDaoCo;
import org.foodup.persistence.entity.FoodUp;
import org.foodup.utils.DataResponse;

@Path("/foodups")
@Produces("application/json")
public class FoodUpResource {

	@GET
	@Path("{id}")
	public Response getFoodUp(@PathParam("id") Integer id) {

		FoodUpDaoCo foodUpDao = new FoodUpDaoCo();
		DataResponse<FoodUp> response = new DataResponse<>();
		FoodUp foodUp = foodUpDao.getFoodUp(id);

		if (foodUp != null) {
			response.setData(foodUp);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	public Response getFoodUps(@Context ContainerRequestContext crc) {

		Integer idUser = (Integer) crc.getProperty("idUser");

		FoodUpDaoCo foodUpDao = new FoodUpDaoCo();
		DataResponse<List<FoodUp>> response = new DataResponse<>();
		List<FoodUp> foodUps = foodUpDao.getListFoodUpsByUser(idUser);

		if (foodUps != null) {
			response.setData(foodUps);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@POST
	public Response createFoodUp(@Context ContainerRequestContext crc, FoodUp foodUp) {

		Integer idUser = (Integer) crc.getProperty("idUser");

		FoodUpDaoCo foodUpDao = new FoodUpDaoCo();
		DataResponse<FoodUp> response = new DataResponse<>();

		if (foodUpDao.createFoodUp(idUser, foodUp)) {
			
			if (foodUpDao.createMenu(foodUp)) {
				response.setData(foodUp);

				return Response.ok(response).build();
			}

			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Status.BAD_REQUEST).build();

	}
	
	@POST
	@PermitAll
	@Path("/explorer")
	public Response createFoodUpExplorer(FoodUp foodUp) {

		FoodUpDaoCo foodUpDao = new FoodUpDaoCo();
		DataResponse<FoodUp> response = new DataResponse<>();

		if (foodUpDao.createFoodUpExplorer(foodUp)) {
			
			if (foodUpDao.createMenu(foodUp)) {
				response.setData(foodUp);

				return Response.ok(response).build();
			}

			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteFoodUp(@Context ContainerRequestContext crc, @PathParam("id") Integer idFoodUp) {
		
		
		Integer idUser = (Integer) crc.getProperty("idUser");

		if (idFoodUp != null) {
			
			FoodUpDaoCo foodUpDao = new FoodUpDaoCo();
			DataResponse<Boolean> response = new DataResponse<>();

			if (foodUpDao.deleteFoodUp(idUser, idFoodUp)) {
				response.setData(true);
				return Response.ok(response).build();
			}

			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@DELETE
	@Path("/meals/{id}/dishes/{id-dish}")
	public Response deleteDishMeal(@Context ContainerRequestContext crc, @PathParam("id") Integer idMeal, @PathParam("id-dish") Integer idDish) {
		
		//Integer idUser = (Integer) crc.getProperty("idUser");

		if (idMeal != null && idDish != null) {
			
			MealDaoCo mealDao = new MealDaoCo();
			DataResponse<Boolean> response = new DataResponse<>();

			if (mealDao.deleteDishMeal(idMeal, idDish)) {
				response.setData(true);
				return Response.ok(response).build();
			}

			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@PUT
	@Path("/meals/{id}/dishes/{id-dish}/{new-id-dish}")
	public Response updateDishMeal(@PathParam("id") Integer idMeal, @PathParam("id-dish") Integer idDish, @PathParam("new-id-dish") Integer newIdDish) {

		if (idMeal != null && idDish != null && newIdDish != null) {
			
			MealDaoCo mealDao = new MealDaoCo();
			DataResponse<Boolean> response = new DataResponse<>();

			if (mealDao.updateDishMeal(idMeal, idDish, newIdDish)) {
				response.setData(true);
				return Response.ok(response).build();
			}

			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		return Response.status(Status.BAD_REQUEST).build();
	}
}
