package org.foodup.resources;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.foodup.persistence.dao.DishDaoCo;
import org.foodup.persistence.entity.Dish;
import org.foodup.utils.DataResponse;

@Path("/dishes")
@Produces("application/json")
public class DishResource {

	@GET
	public Response getDishes(@Context ContainerRequestContext crc, @QueryParam("name") String name) {

		Logger.getLogger(DishResource.class.getName()).log(Level.INFO, "Se accede al recurso de recoger plato");
		
		Integer id = (Integer) crc.getProperty("idUser");

		DishDaoCo dishDao = new DishDaoCo();
		DataResponse<List<Dish>> response = new DataResponse<>();
		List<Dish> dishes = null;

		if (name == null) {
			dishes = dishDao.getAllDishes(id);
		} else {
			// dishes = dishDao.getAllDishesByName(name);
		}

		if (dishes != null) {
			response.setData(dishes);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	@PermitAll
	@Path("/explorer")
	public Response getDishesExplorer() {

		Logger.getLogger(DishResource.class.getName()).log(Level.INFO, "Se accede al recurso de recoger plato");
		
		Integer id = 0;

		DishDaoCo dishDao = new DishDaoCo();
		DataResponse<List<Dish>> response = new DataResponse<>();
		List<Dish> dishes = null;

		dishes = dishDao.getAllDishes(id);

		if (dishes != null) {
			response.setData(dishes);
			return Response.ok(response).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@PUT
	public Response updateDish(@Context ContainerRequestContext crc, Dish dish) {
		
		Logger.getLogger(DishResource.class.getName()).log(Level.INFO, "Se accede al recurso de actualizar plato");

		Integer idUser = (Integer) crc.getProperty("idUser");

		DishDaoCo dishDao = new DishDaoCo();
		DataResponse<Dish> response = new DataResponse<>();

		if (dish != null && idUser.equals(dish.getIdUserOwner())) {
			if (dishDao.updateDish(idUser, dish)) {
				response.setData(dish);
				return Response.ok(response).build();
			}
			return Response.status(Status.CONFLICT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@POST
	public Response createDish(@Context ContainerRequestContext crc, Dish dish) {
		
		Logger.getLogger(DishResource.class.getName()).log(Level.INFO, "Se accede al recurso de crear plato");

		Integer idDish = (Integer) crc.getProperty("idUser");

		DishDaoCo dishDao = new DishDaoCo();
		DataResponse<Dish> response = new DataResponse<>();

		if (dish != null) {
			if (dishDao.createDish(idDish, dish)) {
				response.setData(dish);
				return Response.ok(response).build();
			}
			return Response.status(Status.CONFLICT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteDish(@Context ContainerRequestContext crc, @PathParam("id") Integer idDish) {
		
		Integer idUser = (Integer) crc.getProperty("idUser");

		if (idDish != null) {
			
			DishDaoCo dishDao = new DishDaoCo();
			DataResponse<Boolean> response = new DataResponse<>();

			if (dishDao.deleteDish(idUser, idDish)) {
				response.setData(true);
				return Response.ok(response).build();
			}

			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		return Response.status(Status.BAD_REQUEST).build();
	}

}
