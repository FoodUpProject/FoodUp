package org.foodup.utils;

/**
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

public class ValidationUtils {
    
	private String secreto = "secreto";

    public String[] decode(String credential) {
        //Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
    	credential = credential.replaceFirst("Basic ", "");
 
        //Decode the Base64 into byte[]
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(credential);
 
        //If the decode fails in any case
        if(decodedBytes == null || decodedBytes.length == 0){
            return null;
        }
 
        //Now we can convert the byte[] into a splitted array :
        //  - the first one is email,
        //  - the second one password
        return new String(decodedBytes).split(":");
    }
	
	public String generateHashSha256(String message) {
		
		String hash = null;
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.reset();
			byte[] buffer = message.getBytes("UTF-8");
			md.update(buffer);
			byte[] digest = md.digest();
			
			StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < digest.length; i++) {
		        sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
		    }
		    
		    hash = sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			Logger.getLogger(ValidationUtils.class.getName()).log(Level.SEVERE, null, e);
		} catch (UnsupportedEncodingException e) {
			Logger.getLogger(ValidationUtils.class.getName()).log(Level.SEVERE, null, e);
		}

		return hash;
	}
	
	public String generateToken(long time, String token) {
		return generateHashSha256(time + secreto + token);
	}
    
}
